import pandas as pd
from oncopad import to_dict


class Datasets(object): # Load datasets

    def __init__(self):

        #Tumor type annotations
        self.ttypes = pd.read_csv('static/data/TTYPE_annotations.tsv', sep='\t')

        #Gene - Tumor type annotations
        self.dfgttype = pd.read_csv('static/data/TTYPE_alldrivers.tsv', sep='\t')

        self.dfg_biom_ttype = pd.read_csv('static/data/TTYPE_genes_biomarkers.tsv', sep='\t')

        #Gene annotations
        self.dfGENEatt = pd.read_csv('static/data/genes_role.txt', sep='\t')
        self.CDSlen_selected_transcript = pd.read_csv('static/data/ensembl70_cdslen_selectedtranscripts.tsv', sep='\t', index_col = False)
        self.CDSlen_selected_transcript = self.CDSlen_selected_transcript.rename(columns={'SYMBOL':'Gene symbol'})
        self.gene_exons = pd.read_csv('static/data/ensembl70_gene_exoncoords.tsv', sep='\t', index_col = False)
        self.allsymbols = pd.read_csv('static/data/ensembl70_all_symbols.txt', sep='\t', index_col = False)['SYMBOL'].tolist()
        self.gene_clonality = pd.read_csv('static/data/TTYPE_clonal_drivers.tsv', sep='\t', index_col = False)


        #Intogen sample data
        self.dfsample = pd.read_csv('static/data/intogen_v3_pams_nosplicing_allcoords_CLLcohort.tsv', sep='\t', index_col=False, header=0)
        self.dfsample  = self.dfsample.drop_duplicates()
        self.dfsample = self.dfsample.rename(columns={'SYMBOL':'Gene symbol'})

        self.dfsample_prescriptions = pd.read_csv('static/data/intogen_pams_cohort_CGI_output_parsed.tsv', sep='\t', index_col=False, header=0)

        with open("static/data/synonims_gene_symbol.tsv") as fd:
            self.allsymbols_synonyms = dict(line.strip().split('\t') for line in fd)

    def tumor_types_without_pan(self) -> dict:
        return to_dict(self.ttypes[self.ttypes.TTYPE != 'PAN'])

    def tumor_types_with_pan(self) -> dict:
        return to_dict(self.ttypes)

    def sample_pams(self) -> pd.DataFrame:
        return self.dfsample

    def panel_data(self, ttype) -> pd.DataFrame:
        return self.example_panel_data[ttype]

    def load_CDSlen(self) -> pd.DataFrame:
        return self.dfmerged.drop_duplicates()







