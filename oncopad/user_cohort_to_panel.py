import pandas as pd

def reformat_vep(file_splitted, cancer_types):
    file_splitted = file_splitted[1:]
    file_splitted_fields = [line.split('\\t') for line in file_splitted]
    file_splitted_fields = [row for row in file_splitted_fields if row[1] != 'Location']

    cohort_df_total = pd.DataFrame()

    for cancer_type in cancer_types:
        cohort_df = pd.DataFrame()
        cohort_df['SAMPLE'] = [row[0] for row in file_splitted_fields]
        cohort_df['ENSG'] = [row[3] for row in file_splitted_fields]
        cohort_df['Gene symbol'] = [ extra_f.split('=')[1] for row_extra_f in [row[13].split(';') for row in file_splitted_fields] for extra_f in row_extra_f if 'SYMBOL=' in extra_f]
        cohort_df['CHR'] = [row[1].split(':')[0] for row in file_splitted_fields]
        cohort_df['STRAND_p'] = [ int(extra_f.split('=')[1]) for row_extra_f in [row[13].split(';') for row in file_splitted_fields] for extra_f in row_extra_f if 'STRAND=' in extra_f]

        def change_strand(row):
            if row['STRAND_p'] == -1:
                row['STRAND'] = '-'
            else:
                row['STRAND'] = '+'
            return row

        cohort_df = cohort_df.apply(change_strand,axis=1)

        cohort_df['START'] = [row[1].split(':')[1].split('-')[0] for row in file_splitted_fields]
        cohort_df['ALLELE'] = [row[2] for row in file_splitted_fields]
        cohort_df['PROTEIN_POS'] = [row[9] for row in file_splitted_fields]
        cohort_df['AA_CHANGE'] = [row[10] for row in file_splitted_fields]
        cohort_df['CT'] = [row[6] for row in file_splitted_fields]

        cohort_df['TTYPE'] = cancer_type

        cohort_df_total = cohort_df_total.append(cohort_df[['TTYPE','SAMPLE','Gene symbol','ENSG','CHR','STRAND','START','ALLELE','PROTEIN_POS','AA_CHANGE','CT']])

    return cohort_df_total