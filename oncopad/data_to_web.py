import pandas as pd
from tempfile import mktemp
import re
from pandas import ExcelWriter


def summary_table(requirements, panel, dfttypedesc, dfgenedriverttype, tiergenes, genes_not_found_l, flags, trimmer_customized):
    summary = {}

    if trimmer_customized:
        summary['trimmer_customized'] = '1'

    for f in flags:
        if flags[f]:
            summary[f] = '1'

    def list_to_str(genelist):
        if len(genelist) == 1:
            list_str = str(genelist[0]) + ' was'
        else:
            list_str = ', '.join(genelist[:-1])
            list_str += ' and '+str(genelist.pop())+ ' were'
        return list_str


    if len(genes_not_found_l) != 0:
        summary['genes_not_found'] = list_to_str(genes_not_found_l)

    for gene_req in ['mandatory_genes','excluded_genes']:
        if len(requirements[gene_req]) > 0:
            genes_inc_exc_str = list_to_str(requirements[gene_req])
            summary[gene_req] = genes_inc_exc_str

            genes_not_mutated = [g for g in requirements[gene_req] if g not in panel['Gene symbol'].tolist()]
            if len(genes_not_mutated) > 0:
                genes_not_muy_str = list_to_str(genes_not_mutated)
                summary['genes_not_mutated'] = genes_not_muy_str

    for maxnum in ['max_kbp','max_g']:
        if str(requirements[maxnum]) != '':
            summary[maxnum] = requirements[maxnum]

    desc = ''
    totaldesc  = ''
    dfttypedesc = dfttypedesc.reset_index()
    shortened = False

    for index in dfttypedesc.index.tolist():
        if index <= 4:
            if index == 0:
                desc = dfttypedesc['DESC'][index]+' ('+dfttypedesc['TTYPE'][index]+')'
            else:
                if index != len(dfttypedesc)-1:
                    desc += ', '+dfttypedesc['DESC'][index]+' ('+dfttypedesc['TTYPE'][index]+')'
                else:
                    desc += ' and ' +dfttypedesc['DESC'][index]+' ('+dfttypedesc['TTYPE'][index]+')'
        else:
           desc += 'and '+str(len(dfttypedesc)-5)+' cancer types more'
           shortened = True
           break


    summary['ttype_desc_short'] = desc

    if shortened:
        for index in dfttypedesc.index.tolist():
            if index == 0:
                totaldesc = dfttypedesc['DESC'][index]+' ('+dfttypedesc['TTYPE'][index]+')'
            else:
                if index != len(dfttypedesc)-1:
                    totaldesc += ', '+dfttypedesc['DESC'][index]+' ('+dfttypedesc['TTYPE'][index]+')'
                else:
                    totaldesc += ' and ' +dfttypedesc['DESC'][index]+' ('+dfttypedesc['TTYPE'][index]+')'

        summary['ttype_desc_long'] = totaldesc
    else:
        summary['ttype_desc_long'] = 'FALSE'

    summary['panel_candidates'] = len(set(dfgenedriverttype[dfgenedriverttype['TTYPE'].isin(dfttypedesc['TTYPE'])]['Gene symbol'].tolist()))
    summary['tier1'] = len(tiergenes['Tier1'])
    summary['tier2'] = len(tiergenes['Tier2'])
    summary['max_coverage_tier1'] = str(round(max( panel[panel['Gene symbol'].isin(tiergenes['Tier1'])]['Accumulative frequency PAN'].tolist() ),2)*100)+'%'
    if len(tiergenes['Tier2']) != 0:
        summary['max_coverage_tier2'] = str(round(max( panel[panel['Gene symbol'].isin(tiergenes['Tier2'])]['Accumulative frequency PAN'].tolist() ),2)*100)+'%'
    else:
        summary['max_coverage_tier2'] = 0
    summary['max_kbp_tier1'] = str(round(max( panel[panel['Gene symbol'].isin(tiergenes['Tier1'])]['Kbp_cumulative'].tolist() ),2))
    summary['total_samples'] = sum(dfttypedesc['TOTAL'].tolist())
    summary['panel_name'] = requirements['panel_name']
    summary['ttype_select'] = requirements['ttype_select']
    return summary


def data_muts_needle_plot(data_to_table, dfexons, dfregions, gene_region_criteria, dfsample, temp_dir, wholegenes):
    data_to_plot_regions = {}
    data_to_plot_regions['Tier1'] = {}
    data_to_plot_regions['Tier2'] = {}
    data_to_table_12 = data_to_table[data_to_table['Tier']!='Tier3']
    if wholegenes:
        genes_names = data_to_table_12['Gene'].tolist()
    else:
        genes_names = list(set([gene.split(' ')[0] for gene, dfgene in data_to_table_12.groupby('Gene')]))



    for gene in genes_names:

        if gene in dfexons['Gene symbol'].tolist():
            tiers = data_to_table_12[data_to_table_12['Gene'].str.contains(gene)]['Tier'].tolist()
            if 'Tier1' in tiers:
                tier = 'Tier1'
            else:
                tier = 'Tier2'

            data_to_plot_regions[tier][gene] = {}
            filepath = mktemp(dir=temp_dir)

            for filetogenerate in ['regions','muts','color']:

                if filetogenerate == 'regions':
                    filepathr = filepath+'_regions.json'
                    with open(filepathr,'w') as fOUT:
                        if gene_region_criteria[gene] == 'whole_gene':
                            fOUT.writelines('[]')
                        else:
                            fOUT.writelines('[')
                            region_gene =  dfregions[dfregions['Gene symbol'] == gene]
                            region_gene = region_gene.reset_index(drop=True)
                            for index in region_gene.index.tolist():
                                if index == 0:
                                    fOUT.writelines('{')
                                else:
                                    fOUT.writelines(',{')

                                fOUT.writelines('"name": "'+region_gene['CLUSTER_NAME'][index].split('(')[0]+'", "coord": "'+str(int(region_gene['PROT_START'][index]))+'-'+str(int(region_gene['PROT_END'][index]))+'"}')
                            fOUT.writelines(']')

                    filepathr = re.sub(temp_dir,'temp',filepathr)
                    data_to_plot_regions[tier][gene][filetogenerate] = filepathr

                elif filetogenerate =='color':
                    data_to_plot_regions[tier][gene][filetogenerate] = []
                    if gene_region_criteria[gene] != 'whole_gene':
                        region_gene = dfregions[dfregions['Gene symbol'] == gene]
                        region_gene = region_gene.reset_index(drop=True)
                        for index in region_gene.index.tolist():
                            data_to_plot_regions[tier][gene][filetogenerate].append([region_gene['CLUSTER_NAME'][index],'yellow'])
                else:

                    filepathg = filepath+'.json'
                    with open(filepathg,'w') as fOUT:
                        mutations_gene = dfsample[dfsample['Gene symbol']==gene][['PROTEIN_POS','CT']]
                        fOUT.writelines('[')
                        mutations_gene = mutations_gene.reset_index(drop=True)
                        index = 0
                        for mut,dfmut in mutations_gene.groupby(['PROTEIN_POS','CT']):
                            if mut[0] != '.':
                                coord = int(mut[0].split('.')[0])
                                value = len(dfmut)
                                category = mut[1]

                                if str(coord) != '\\N':
                                    if index == 0:
                                        fOUT.writelines('{')
                                    else:
                                        fOUT.writelines(',{')


                                    fOUT.writelines('"coord": "'+str(int(coord))+'", "category": "'+category+'", "value": "'+str(value)+'"}')
                                    index +=1

                        fOUT.writelines(']')

                    filepathg = re.sub(temp_dir,'temp',filepathg)
                    data_to_plot_regions[tier][gene][filetogenerate] = filepathg

            maxcoord = dfexons[dfexons['Gene symbol']==gene]['CDSlen'].tolist()[0] / float(3)
            data_to_plot_regions[tier][gene]['maxcoord'] = int(maxcoord)

    return data_to_plot_regions


def parse_data_for_web(dfpanel, tiergenes, dfgeneatt, dfttypedesc, dfregions, wholegenes, gene_region_criteria, temp_dir, dfexons, dfsample, prescriptions_count, prescriptions_items, gene_clonality):
    flags = {}
    data_to_plot = {}

    #1) Process gene attributes
    dfgenes_tiers = []
    for tier, genes in tiergenes.items():
        for gene in genes:
            dfgenes_tiers.append({'Tier':tier,'Gene symbol':gene})
    dfgenes_tiers = pd.DataFrame(dfgenes_tiers)

    if len(dfregions) == 0:
        wholegenes = True

    if wholegenes:
        dfgeneatt = dfgeneatt[dfgeneatt['Gene symbol'].isin(dfpanel['Gene symbol'].tolist())]
        dfgeneatt_complete = pd.merge(dfgeneatt,dfexons[['Gene symbol','ENST']].drop_duplicates(),on='Gene symbol')
        dfgeneatt_corrected = pd.merge(dfgeneatt_complete, dfgenes_tiers, on = 'Gene symbol')

    else:
        geneatt_corrected_hs = []
        genes_hs_panel = []
        for gene, dfgene in dfregions.groupby('Gene symbol'):
            if gene_region_criteria[gene] != 'whole_gene':
                if len(dfgene) > 0:
                    gene_tiers = dfgenes_tiers[dfgenes_tiers['Gene symbol'].isin(dfgene['GENE_CLUSTER_NAME'].tolist())]
                    clusters = dfgene['GENE_CLUSTER_NAME'].tolist()
                    tiers_no3 = [ t for t in gene_tiers['Tier'].tolist() if t != 'Tier3']
                    if len(tiers_no3) > 0:
                        gene_tiers_names = '&'.join(sorted(list(set(tiers_no3))))

                        role = dfgeneatt[dfgeneatt['Gene symbol'] == gene]['Role'].tolist()[0]
                        driverorigin = dfgeneatt[dfgeneatt['Gene symbol'] == gene]['Driver origin'].tolist()[0]
                        driverorigin = [do for do in set(driverorigin) if str(do)!='nan']
                        enst = dfexons[dfexons['Gene symbol'] == gene]['ENST'].tolist()[0]

                        geneatt_corrected_hs.append({'Gene symbol':gene, 'Driver origin':driverorigin, 'Role':role, 'Tier':gene_tiers_names,'ENST':enst})
                        genes_hs_panel.extend(clusters)


        dfgeneatt_complete = pd.merge(dfgeneatt,dfexons[['Gene symbol','ENST']].drop_duplicates(),on='Gene symbol')
        dfgeneatt_corrected_partial = pd.merge(dfgeneatt_complete, dfgenes_tiers, on = 'Gene symbol')
        dfgeneatt_corrected_partial = dfgeneatt_corrected_partial[~ dfgeneatt_corrected_partial['Gene symbol'].isin( [x for sublist in genes_hs_panel for x in sublist])]
        dfgeneatt_corrected = dfgeneatt_corrected_partial.append(pd.DataFrame(geneatt_corrected_hs))


    dfgeneatt_corrected_tier12 = dfgeneatt_corrected[dfgeneatt_corrected['Tier']!='Tier3']

    #Add clonality feature
    gene_clonality = gene_clonality[gene_clonality['CLONAL_TTYPE'].isin(dfttypedesc['TTYPE'].tolist())]
    gene_clonality_label = []
    for gene in dfgeneatt_corrected_tier12['Gene symbol'].tolist():
        dfgene = gene_clonality[gene_clonality['Gene symbol']==gene]
        if len(dfgene) > 0:
            clonal_ttype = ','.join(dfgene['CLONAL_TTYPE'].tolist())
            clonal_ttype_fullname = ','.join([dfttypedesc[dfttypedesc['TTYPE']==ttype]['DESC'].tolist()[0] for ttype in dfgene['CLONAL_TTYPE'].tolist()])
        else:
            clonal_ttype = ''
            clonal_ttype_fullname = ''

        gene_clonality_label.append({'Gene symbol':gene,'Clonal_ttype':clonal_ttype,'Clonal_ttype_fullname':clonal_ttype_fullname})

    if len(gene_clonality_label) == 0:
        dfgeneatt_corrected_tier12_clonality = dfgeneatt_corrected_tier12
        dfgeneatt_corrected_tier12_clonality['Major driver'] = 'Unknown'
    else:
        dfgeneatt_corrected_tier12_clonality = pd.merge(dfgeneatt_corrected_tier12, pd.DataFrame(gene_clonality_label), on='Gene symbol')
    dfgeneatt_corrected_tier12_clonality['Driver origin str'] = dfgeneatt_corrected_tier12_clonality['Driver origin'].apply( lambda x: ', '.join([str(xx) for xx in x if str(xx)!='nan']))

    flags['DISMIS_CUM_MOREONEGENE'] = False
    # 2) Data to plot
    for tier in tiergenes.keys():
        data_to_plot[tier] = {}

        panel_tier_ordered = dfpanel[dfpanel['Gene symbol'].isin(tiergenes[tier])].sort_values(by='index')

        data_to_plot[tier]['Genes'] = panel_tier_ordered['Gene symbol'].tolist()
        data_to_plot[tier]['Cumulative'] = panel_tier_ordered['Accumulative frequency PAN'].tolist()
        if 'Cumulative_2' in panel_tier_ordered.columns.tolist():
            if sum(panel_tier_ordered['Cumulative_2'].tolist()) != 0:
                data_to_plot[tier]['Cumulative_2'] = panel_tier_ordered['Cumulative_2'].tolist()
                if 'Cumulative_3' in panel_tier_ordered.columns.tolist():
                    data_to_plot[tier]['Cumulative_3'] = panel_tier_ordered['Cumulative_3'].tolist()
            else:
                flags['DISMIS_CUM_MOREONEGENE'] = True
        else:
            flags['DISMIS_CUM_MOREONEGENE'] = True


        data_to_plot[tier]['Frequency'] = {}

        for ttype in dfttypedesc['TTYPE'].tolist():
            data_to_plot[tier]['Frequency'][ttype] = panel_tier_ordered[ttype+'_Frequency'].tolist()

    flags['DISMIS_DRUGS'] = False
    if len(prescriptions_items) == 0:
        flags['DISMIS_DRUGS'] = True
        prescriptions_items_m = pd.DataFrame()
    else:
        prescriptions_items_m = pd.merge(prescriptions_items,dfgenes_tiers, on='Gene symbol')
        prescriptions_items_m = prescriptions_items_m[prescriptions_items_m['Tier']!='Tier3']

    data_to_table = pd.merge(dfpanel,dfgenes_tiers, on='Gene symbol')
    data_to_table_m = pd.merge(data_to_table, prescriptions_count, on='Gene symbol', how='left')


    data_to_table = data_to_table_m.rename(columns={'Gene symbol':'Gene','Accumulative frequency PAN':'Cumulative'})


    for ttype in dfttypedesc['TTYPE'].tolist():
        data_to_table = data_to_table.rename(columns={ttype+'_Frequency':ttype})


    #2.1) Data to plot prescriptons
    for tier in ['Tier1','Tier2']:
        genes_ordered = dfpanel[dfpanel['Gene symbol'].isin(tiergenes[tier])].sort_values(by='index')['Gene symbol'].tolist()
        data_to_plot[tier]['Unknown'] = []
        data_to_plot[tier]['Known oncogeneicity'] = []
        data_to_plot[tier]['Sensitivity'] = []
        data_to_plot[tier]['Resistance'] = []

        for gene in genes_ordered:
            prescriptions_count_item = prescriptions_count[prescriptions_count['Gene symbol'] == gene]
            total_mut = (prescriptions_count_item['Total_muts'].tolist()[0])
            rel_muts = sum([prescriptions_count_item['DSens_count'].tolist()[0],prescriptions_count_item['DResis_count'].tolist()[0],prescriptions_count_item['Known_count'].tolist()[0]])
            if rel_muts == total_mut:
                data_to_plot[tier]['Unknown'].append(0)
            else:
                data_to_plot[tier]['Unknown'].append((total_mut-rel_muts)/float(total_mut))

            data_to_plot[tier]['Sensitivity'].append(prescriptions_count_item['DSens_count'].tolist()[0]/float(total_mut))
            data_to_plot[tier]['Resistance'].append(prescriptions_count_item['DResis_count'].tolist()[0]/float(total_mut))
            data_to_plot[tier]['Known oncogeneicity'].append(prescriptions_count_item['Known_count'].tolist()[0]/float(total_mut))

    #2.2) Muts needle plot
    if len(dfregions) > 0:
        dfregions_tier12 = dfregions[dfregions['GENE_CLUSTER_NAME'].isin(tiergenes['Tier1']) | dfregions['GENE_CLUSTER_NAME'].isin(tiergenes['Tier2'])]
    else:
        dfregions_tier12 = ''
    data_to_plot_regions = data_muts_needle_plot(data_to_table, dfexons, dfregions_tier12, gene_region_criteria,dfsample, temp_dir, wholegenes)

    #3) Data to table
    data_to_table = data_to_table.fillna('')
    data_to_table['index_record'] = data_to_table.index.tolist()
    data_to_tabledict = data_to_table.to_dict(orient='records')
    data_to_tabledict_corrected = []
    for entry in data_to_tabledict:
        for k in entry.keys():
            if k == 'CDSlen':
                if entry[k] != '':
                    entry[k] = int(entry[k])

        data_to_tabledict_corrected.append(entry)

    return data_to_plot, data_to_table, data_to_tabledict_corrected, data_to_plot_regions, dfgeneatt_corrected_tier12_clonality, prescriptions_items_m, flags


def BEDfile(data, dfregions, temp_dir, requirements, gene_exons):
    data = pd.DataFrame(data)
    data = data[data['Tier'].isin(['Tier1','Tier2'])]

    filepath = mktemp(dir=temp_dir)
    with open(filepath+'.bed','w') as fOUT:
        fOUT.writelines('track name="'+requirements['panel_name']+'" description="Tier 1 & 2 driver genes and regions. If a gene is included as a whole there is a BED line per gene exon otherwise there is one line per gene region"\n')

    dfregions['CHR_chr'] = dfregions['CHR'].apply(lambda v: 'chr{}'.format(v))
    for gene in data.Gene.tolist():
        if 'Ex' in gene: #If it is a region
            dfregions_tobed = dfregions[dfregions['GENE_CLUSTER_NAME']==gene]
            dfregions_tobed[['CHR_chr','START','END','GENE_CLUSTER_NAME']].to_csv(filepath+'.bed', mode='a',index=False, header=False, sep='\t')
        else:
            gene_exons[gene_exons['SYMBOL']==gene][['CHR','EXON_START','EXON_END','NAME']].to_csv(filepath+'.bed', mode='a',index=False, header=False, sep='\t')

    return filepath.replace(temp_dir + '/', '')+'.bed'


def Excelfile(panel, dfregions, prescriptions_items, data_to_table_genes,  ttypes, temp_dir, requirements):
    filepath = mktemp(dir=temp_dir)
    writer = ExcelWriter(filepath+'.xlsx')
    cols_panel = ['Gene','Tier','Cumulative','Kbp_cumulative']
    for ttype in ttypes:
        cols_panel.append(ttype)
    panel[cols_panel].to_excel(writer,'Mutational coverage')
    cols = ['Gene','Tier']+[c for c in panel.columns.tolist() if 'Cumulative' in c]
    panel[cols].to_excel(writer,'Mutational coverage > 1 gene')

    if len(dfregions) > 0:
        dfregions[['GENE_CLUSTER_NAME','EXON','CLUSTER_PROTEIN_COORDS','CLUSTER_LENGTH','MUTATION_COUNT','MUTATION_FREQ','CDS_PROPORTION','CLUSTER_GENOMIC_COORDS']].to_excel(writer,'Regions details')
    if len(prescriptions_items) > 0:
        prescriptions_items[['Gene symbol','Tier','BIOMARKER','GENERAL_ASSOCIATION','DRUG','EVIDENCES_TTYPE','MUT_COUNT','MUT_PROP']].to_excel(writer,'Relevant mutations')

    data_to_table_genes = data_to_table_genes.rename(columns={'Driver origin str':'Driver origin','Clonal_ttype':'Major driver'})
    data_to_table_genes[['Gene symbol','Tier','Role','Major driver','Driver origin']].to_excel(writer,'General features')
    writer.save()

    return filepath.replace(temp_dir + '/', '')+'.xlsx'