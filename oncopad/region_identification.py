import functools
from multiprocessing.pool import Pool

import pandas as pd
import oncopad.locateClusters as locateClusters


def acumulative_frequency_sorting(gene_clusters, gene, gene_total_muts, gene_CDS_enst):
    merged_samples = []
    indexes = []
    gene_clusters_to_update = gene_clusters.copy(deep=True)
    gene_clusters_to_update['MUTATION_COUNT_CUMULATIVE'] = gene_clusters_to_update['MUTATION_COUNT']
    cluster_len_cumulative = 0
    for index in gene_clusters_to_update.index.tolist():
        gene_clusters_to_update = gene_clusters_to_update[~ gene_clusters_to_update.index.isin(indexes)]

        if len(gene_clusters_to_update) >= 1:
            for index2 in gene_clusters_to_update.index.tolist():
                gene_clusters_to_update['SAMPLES'][index2] = list(set(gene_clusters_to_update['SAMPLES'][index2])-set(merged_samples))
                gene_clusters_to_update['MUTATION_COUNT_CUMULATIVE'][index2] = len(gene_clusters_to_update['SAMPLES'][index2])

            max_mutation_count = max(gene_clusters_to_update['MUTATION_COUNT_CUMULATIVE'].tolist())
            max_mutation_count_index = gene_clusters_to_update[gene_clusters_to_update['MUTATION_COUNT_CUMULATIVE']==max_mutation_count].index.tolist()[0]
            indexes.append(max_mutation_count_index)

            merged_samples.extend(gene_clusters_to_update['SAMPLES'][max_mutation_count_index])

            cluster_len_cumulative += gene_clusters_to_update['CLUSTER_LENGTH'][max_mutation_count_index]

            gene_clusters.loc[max_mutation_count_index, 'MUTATION_COUNT_CUMULATIVE'] = max_mutation_count
            gene_clusters.loc[max_mutation_count_index, 'MUTATION_FREQ_CUMULATIVE'] = len(merged_samples)/float(gene_total_muts)
            gene_clusters.loc[max_mutation_count_index, 'CUMULATIVE_CDS_PROPORTION'] = ( cluster_len_cumulative/float(gene_CDS_enst) )
            gene_clusters.loc[max_mutation_count_index, 'CLUSTER_CDS_PROPORTION'] = ( gene_clusters_to_update['CLUSTER_LENGTH'][max_mutation_count_index] /float(gene_CDS_enst) )

    return gene_clusters


def find_best_window(pos, size=50):

    best_count = 0
    for start in range(len(pos)):
        count = 1
        end = start + 1
        for end in range(start + 1, len(pos)):
            if (pos[end] - pos[start] + 1) > size:
                break
            count += 1
        if count > best_count:
            best_count = count
            best_window = pos[start:end]

    return best_window

def find_windows(pos, coverage=80, size=50):
    pos = sorted(pos)
    maxcov= len(pos)
    target = min(int(len(pos)*coverage/100),len(pos))
    windows = []
    cover = 0
    while cover < target and len(pos) > 0:
        window = find_best_window(pos, size=size)
        windows.append(window)
        cover += len(window)
        pos = [p for p in pos if p not in window]

    return [w for w in windows if len(w)>1]


def region_identification(gene_CDS_enst, dfsamples, dfttypedesc):

    genes_of_interest = gene_CDS_enst['Gene symbol'].drop_duplicates().tolist()
    dfsamples =dfsamples.rename(columns={'START':'START_noint'})
    dfsamples['START'] = dfsamples['START_noint'].apply(lambda x: int(x))
    dfsamples = dfsamples[dfsamples['TTYPE'].isin(dfttypedesc['TTYPE'].tolist())]

    counting_clusters ={}
    steps = [0, 0.1,0.2,0.3,0.4,0.5, 0.6, 0.7, 0.8, 0.9, 1, 2]
    gene_clusters_total = []

    for step in steps:
        counting_clusters[step] = {}

    if len(genes_of_interest) > 50:
        # Run in multiple cores
        process_gene_partial = functools.partial(process_gene, dfsamples, gene_CDS_enst)
        with Pool(4) as pool:
            for result in pool.map(process_gene_partial, genes_of_interest):
                gene_clusters_total += result
    else:
        # Run in one core
        for gene in genes_of_interest:
            gene_clusters_total += process_gene(dfsamples, gene_CDS_enst, gene)

    df_gene_clusters_total = pd.DataFrame(gene_clusters_total)
    if len(df_gene_clusters_total) > 0:
        df_gene_clusters_total = df_gene_clusters_total[['GENE_CLUSTER_NAME','CLUSTER_NAME','Gene symbol', 'EXON', 'CLUSTER_PROTEIN_COORDS','CLUSTER_GENOMIC_COORDS','SAMPLES','TTYPES','START','END', 'CHR', 'CLUSTER_LENGTH','MUTATION_COUNT','MUTATION_FREQ','CDS_PROPORTION','PROT_START','PROT_END']]

    return df_gene_clusters_total

def process_gene(dfsamples, gene_CDS_enst, gene):
    gene_windows_mapped_annotations = []
    if gene != '':
        gene_total_muts = float(len(set(dfsamples[dfsamples['Gene symbol'] == gene]['SAMPLE'].tolist())))

        if gene_total_muts > 1:
            # Run clusters algortihm by JDeu-Pons
            data_windows = dfsamples[dfsamples['Gene symbol'] == gene][['SAMPLE', 'Gene symbol', 'START']].drop_duplicates()
            windows = find_windows(data_windows['START'].tolist(), coverage=80, size=100)
            if len(windows) != 0:
                gene_windows = []
                for w in windows:
                    gene_windows.append({'GENE': gene, 'START': min(w), 'END': max(w), 'CHR': dfsamples[dfsamples['Gene symbol'] == gene]['CHR'].tolist()[0]})

                # Map clusters from genomic to protein coordinates by MMunz
                gene_CDSlen = gene_CDS_enst[gene_CDS_enst['Gene symbol'] == gene]['CDSlen'].tolist()[0]
                for row in locateClusters.run(gene_windows):

                    # reverse for strand
                    if row['PROT_START'] > row['PROT_END']:
                        start = row['PROT_START']
                        row['PROT_START'] = row['PROT_END']
                        row['PROT_END'] = start

                    # Add extra info columns: SAMPLES, TTYPES, CLUSTER NAME
                    samples_cluster_list = dfsamples[(dfsamples['START'] >= row['START']) & (dfsamples['START'] <= row['END'])].to_dict(orient='list')
                    # samples_cluster_list = {'SAMPLE': [], 'TTYPE': []}
                    row['SAMPLES'] = samples_cluster_list['SAMPLE']
                    row['TTYPES'] = samples_cluster_list['TTYPE']
                    row['MUTATION_COUNT'] = len(samples_cluster_list['SAMPLE'])
                    row['MUTATION_FREQ'] = len(samples_cluster_list['SAMPLE']) / float(gene_total_muts)

                    row['CLUSTER_LENGTH'] = (row['END'] - row['START']) + 1
                    row['EXON'] = 'Ex' + str(row['LOC_START'])

                    if int(row['PROT_START']) == int(row['PROT_END']):
                        row['CLUSTER_NAME'] = 'Ex' + str(row['LOC_START']) + '(aa:' + str(int(row['PROT_START'])) + ')'
                        row['CLUSTER_PROTEIN_COORDS'] = str(int(row['PROT_START']))
                        row['CLUSTER_GENOMIC_COORDS'] = 'chr' + str(row['CHR']) + ':' + str(int(row['START']))
                    else:
                        row['CLUSTER_NAME'] = 'Ex' + str(row['LOC_START']) + '(aa:' + str(int(row['PROT_START'])) + '-' + str(int(row['PROT_END'])) + ')'
                        row['CLUSTER_PROTEIN_COORDS'] = str(int(row['PROT_START'])) + '-' + str(int(row['PROT_END']))
                        row['CLUSTER_GENOMIC_COORDS'] = 'chr' + str(row['CHR']) + ':' + str(int(row['START'])) + '-' + str(int(row['END']))

                    row['GENE_CLUSTER_NAME'] = gene + ' ' + row['CLUSTER_NAME']
                    row['Gene symbol'] = gene
                    row['CDS_PROPORTION'] = row['CLUSTER_LENGTH'] / float(gene_CDSlen)

                    row['START'] = int(row['START'])
                    row['END'] = int(row['END'])
                    row['CLUSTER_LENGTH'] = int(row['CLUSTER_LENGTH'])

                    gene_windows_mapped_annotations.append(row)

    return gene_windows_mapped_annotations