import re

import bglogs
import pandas as pd

from oncopad.user_cohort_to_panel import reformat_vep


logger = bglogs.get_logger(__name__)


def from_file(requirement_file, kargs):
    readed = str(kargs[requirement_file+'_file'].file.read())
    readed = re.sub("b'","",readed)
    readed = readed.split('\\n')
    readed.pop()
    kargs[requirement_file] = list(set(readed))
    kargs[requirement_file+'_file'].file.close()
    return kargs

def parse_requirements_dict(kargs):
    #Parse requirements
    if kargs['panel_name'] == '':
        kargs['panel_name'] = 'My Panel'

    if kargs['multicancertype'] == 'on':
        if isinstance(kargs['ttype_select_multi'],str):
            kargs['ttype_select'] = kargs['ttype_select_multi'].split(',')
        else:
            ttypes_joined = ','.join(kargs['ttype_select_multi'])
            kargs['ttype_select'] = ttypes_joined.split(',')

    if 'customized_cohort_file' in kargs:
        if kargs['customized_cohort_file'].file is not None:
            kargs = from_file('customized_cohort', kargs)
        else:
            del kargs['customized_cohort_file']

    for gene_list in ['excluded_genes','mandatory_genes','whole_genes','customized_genes']:
        if gene_list in kargs:
            if kargs[gene_list] != '':
                if type(kargs[gene_list]) == str:
                    genelist = kargs[gene_list].split(',')
                else:
                    genelist = kargs[gene_list]

                kargs[gene_list] = list(set([str(g).upper().replace(' ','') for g in genelist if g !='']))

        if gene_list+'_file' in kargs:
            if kargs[gene_list+'_file'].file is not None:
                kargs = from_file(gene_list, kargs)
            del kargs[gene_list+'_file']

    if 'regions' not in kargs:
        kargs['regions'] = 'on'

    return kargs

def check_genes_symbols(list_of_genes, allsymbols_synonyms, allsymbols, genes_not_found_l):
    genes_not_found_l = []
    list_of_genes_ok = []
    for g in list_of_genes:
        if g in allsymbols:
            list_of_genes_ok.append(g)
            continue
        else:
            if g in allsymbols_synonyms.keys():
                list_of_genes_ok.append(allsymbols_synonyms[g])
                continue
            else:
                genes_not_found_l.append(g)

    return list(set(list_of_genes_ok)), genes_not_found_l

def consider_forced_genes(requirements, drivers, dfgeneatt_trimmed_merged, dfgeneatt, allsymbols_synonyms, allsymbols, genes_not_found_l):
    #Add mandatory genes
    if requirements['mandatory_genes'] != '':
        #Check if they are already in panel drivers
        mandatory_genes = requirements['mandatory_genes']
        mandatory_genes_checked, genes_not_found_l = check_genes_symbols(mandatory_genes, allsymbols_synonyms, allsymbols, genes_not_found_l)
        if len(mandatory_genes_checked) != 0:
            already_drivers = [mgene for mgene in mandatory_genes if mgene in drivers]
            mandatory_genes = list(set(mandatory_genes)-set(already_drivers))

            if mandatory_genes:
                #Check if they are drivers in other tumor types
                mandatory_genes_geneatt = dfgeneatt[dfgeneatt['Gene symbol'].isin(mandatory_genes)]
                mandatory_genes_geneatt['Driver origin'] = ['User input']
                if mandatory_genes_geneatt.empty == False:
                    dfgeneatt_trimmed_merged = dfgeneatt_trimmed_merged.append(mandatory_genes_geneatt)
                    if len(mandatory_genes_geneatt) != len(mandatory_genes):
                        mandatory_genes = list(set(mandatory_genes)-set(mandatory_genes_geneatt['Gene symbol'].tolist()))
                        nodrivers = True
                    else:
                        nodrivers = False
                else:
                    nodrivers = True
                #If some of them are drivers and other don't
                if nodrivers:
                    for mgene in mandatory_genes:
                        dfgeneatt_trimmed_merged = dfgeneatt_trimmed_merged.append(pd.DataFrame([{'Gene symbol':mgene,'Role':'No class','Driver origin':['User input']}]))

    #Remove excluded genes
    if requirements['excluded_genes'] != '':
        excluded_genes_checked, genes_not_found_l = check_genes_symbols(requirements['excluded_genes'], allsymbols_synonyms, allsymbols, genes_not_found_l)
        if len(excluded_genes_checked) != 0:
            dfgeneatt_trimmed_merged = dfgeneatt_trimmed_merged[~ dfgeneatt_trimmed_merged['Gene symbol'].isin(excluded_genes_checked)]

    return dfgeneatt_trimmed_merged, genes_not_found_l



def fulfill_genes_input_requirements(dfgeneatt, dfsample, dfgenedriverttype, dfttypedesc, requirements, prescriptions, dfgenesttype_biom, allsymbols, allsymbols_synonyms):

    nogenesfound = False
    trimmer_customized = False
    genes_not_found_l = []

    if 'customized_cohort' in requirements:
        #Parse cancer types
        if ',' in requirements['user_cohort_ttype']:
            requirements['ttype_select'] = requirements['user_cohort_ttype'].split(',')
        else:
            requirements['ttype_select'] = [requirements['user_cohort_ttype']]

        #Reformat tumor cohort
        dfsample = reformat_vep(requirements['customized_cohort'],requirements['ttype_select'])

    #Filter by tumor type
    if isinstance(requirements['ttype_select'],str):
        logger.info('%s' % requirements['ttype_select'])
        requirements['ttype_select'] = [requirements['ttype_select']]

    #Filter genes according panel type
    if requirements['paneltype'] == 'basic':
        dfgenedriverttype_ttype = dfgenedriverttype[dfgenedriverttype['TTYPE'].isin(requirements['ttype_select'])]

    elif requirements['paneltype'] == 'drugprof':
        dfgenedriverttype_ttype = dfgenedriverttype[dfgenedriverttype['TTYPE'].isin(requirements['ttype_select'])]
        dfgenedriverttype_ttype = dfgenedriverttype_ttype.append(dfgenesttype_biom[dfgenesttype_biom['TTYPE'].isin(requirements['ttype_select'])])
        dfgenedriverttype_ttype = dfgenedriverttype_ttype[dfgenedriverttype_ttype['Gene symbol'].isin(dfgenesttype_biom['Gene symbol'].tolist())]
    else:
        todf = []

        if len(requirements['customized_genes']) > 1000:
            requirements['customized_genes'] = requirements['customized_genes'][:999]
            trimmer_customized = True
        for t in requirements['ttype_select']:
            customized_genes_checked, genes_not_found_l = check_genes_symbols(requirements['customized_genes'], allsymbols_synonyms, allsymbols, genes_not_found_l)
            for g in customized_genes_checked:
                    todf.append({'Gene symbol':g,'TTYPE':t})

        dfgenedriverttype_ttype = pd.DataFrame(todf)

        if len(dfgenedriverttype_ttype) == 0:
            return '', '','','',  True, '', ''

    drivers = dfgenedriverttype_ttype['Gene symbol'].drop_duplicates().tolist()

    #Filter gene attributes accordin driver list
    dfgeneatt_trimmed = dfgeneatt[dfgeneatt['Gene symbol'].isin(drivers)]
    drivers_noatt = set(drivers)-set(dfgeneatt_trimmed['Gene symbol'].drop_duplicates().tolist())
    att_no = []
    if len(drivers_noatt) != 0:
        for g in drivers_noatt:
            att_no.append({'Gene symbol':g,'Role':'No class'})
    dfgeneatt_trimmed = dfgeneatt_trimmed.append(pd.DataFrame(att_no))

    if len(drivers) != len(dfgeneatt_trimmed['Gene symbol'].drop_duplicates().tolist()):
        logger.warning('WARNING:check list of driver genes')

    #Add driver origin as driver attribute
    driver_origin = []
    for gene,dfgene in dfgenedriverttype_ttype.groupby('Gene symbol'):
        if requirements['paneltype'] == 'customized':
            origin = ['User input']
        else:
            origin = dfgene['DRIVER_ORIGIN'].drop_duplicates().tolist()
        driver_origin.append({'Gene symbol':gene,'Driver origin':origin})

    dfgeneatt_origin = pd.merge(pd.DataFrame(driver_origin),dfgeneatt_trimmed,on='Gene symbol',how='right')

    #Add mandatory and remove excluded genes
    dfgeneatt_mandatory, genes_not_found_l = consider_forced_genes(requirements,drivers,dfgeneatt_origin,dfgeneatt, allsymbols_synonyms, allsymbols, genes_not_found_l)

    #Update variables to return
    drivers_updated = dfgeneatt_mandatory['Gene symbol'].drop_duplicates().tolist()
    dfsample = dfsample[(dfsample['Gene symbol'].isin(drivers_updated))&(dfsample['TTYPE'].isin(requirements['ttype_select']))]
    dfttypedesc = dfttypedesc[dfttypedesc['TTYPE'].isin(requirements['ttype_select'])]

    #Keep only driver genes that appear mutated
    drivers_mutated_cohort = dfsample['Gene symbol'].drop_duplicates().tolist()
    dfgeneatt_trimmed_merged_incohort = dfgeneatt_mandatory[dfgeneatt_mandatory['Gene symbol'].isin(drivers_mutated_cohort)]
    if len(drivers_mutated_cohort) == 0:
        nogenesfound = True

    stringent_tier1 = False
    if 'stringent_tier1' in requirements:
        stringent_tier1 = True

    return dfsample, drivers_mutated_cohort, dfgeneatt_trimmed_merged_incohort, dfttypedesc, nogenesfound, trimmer_customized, genes_not_found_l, stringent_tier1

def trimmering_panel_length(dfpanel, tiergenes, requirements):
    dfpanel = dfpanel.sort_values(by='Accumulative frequency PAN')
    dfpanel['Kbp_cumulative'] = dfpanel['CDSlen'].cumsum()
    dfpanel['Kbp_cumulative'] = dfpanel['Kbp_cumulative']/float(1000)

    if requirements['max_g'] != '':
        dfpanel = dfpanel[0:int(requirements['max_g'])]

    if requirements['max_kbp'] != '':
        dfpanel = dfpanel[dfpanel['Kbp_cumulative'] <= int(requirements['max_kbp'])]

    for tier in tiergenes.keys():
        tiergenes[tier] = list(set(tiergenes[tier]).intersection(set(dfpanel['Gene symbol'].tolist())))
    return dfpanel, tiergenes

def trimmerting_panel_regions(dfregions, requirements, drivers):
    # 1) Check if gene has regions
    gene_mixed_panel = {}
    if len(dfregions) > 0:
        for gene, dfgene_regions in dfregions.groupby('Gene symbol'):
            if gene in requirements['whole_genes']:
                gene_mixed_panel[gene]='whole_gene'
            else:
                # 2) Check if its regions achieve the desired coverages
                if len(dfgene_regions) > 0:
                    if sum(dfgene_regions['MUTATION_FREQ'].tolist()) >= int(requirements['regions_coverage'])/float(100):
                       gene_mixed_panel[gene]='hotspots'
                    else:
                        gene_mixed_panel[gene]='whole_gene'
                else:
                    gene_mixed_panel[gene]='whole_gene'
    else:
        for g in drivers:
            gene_mixed_panel[g] = 'whole_gene'
    return gene_mixed_panel