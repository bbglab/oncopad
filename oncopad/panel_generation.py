import datetime

import bglogs
import pandas as pd
from ago import human

from oncopad.datasets import Datasets
import oncopad.cumulative_gene_prioritization as panelg
import oncopad.data_to_web as toweb
import oncopad.region_identification as take_regions
import oncopad.cumulative_gene_region_mixed_prioritization as mixed_panel
import oncopad.panel_prescriptions as panel_prescriptions
import oncopad.requirements as requirements_funcs


logger = bglogs.get_logger(__name__)


def generate_panel_with_requirements(data: Datasets, requirements, temp_dir):
    dfttypedesc, dfgeneatt, dfsample, dfgenedriverttype, gene_CDS_enst, gene_exons, allsymbols_list, prescriptions, dfgenesttype_biom, gene_clonality, allsymbols_synonyms = data.ttypes, data.dfGENEatt, data.dfsample, data.dfgttype, data.CDSlen_selected_transcript, data.gene_exons, data.allsymbols,data.dfsample_prescriptions, data.dfg_biom_ttype, data.gene_clonality, data.allsymbols_synonyms

    start = datetime.datetime.now()

    #Trimmer initial data according requirements tumor type, mandatory genes, include and exclude genes
    dfsample, drivers, dfgeneatt, dfttypedesc, user_nogenesfound, trimmer_customized, genes_not_found_l, stringent_tier1 = requirements_funcs.fulfill_genes_input_requirements(dfgeneatt, dfsample, dfgenedriverttype, dfttypedesc, requirements, prescriptions, dfgenesttype_biom, allsymbols_list, allsymbols_synonyms)

    if user_nogenesfound:
        return '', '', '', '', '', '', '', '', '', '', True

    logger.info('>>>>>>>>>>>>>>> Considering %d driver genes (%s)' % (len(drivers), human(start)))

    #Take data of exons
    gene_CDS_enst = gene_CDS_enst[gene_CDS_enst['Gene symbol'].isin(drivers)]

    if len(drivers) == 0: #Previous trimmering left us without genes to consider for the panel -> warning
        return '', '', '', '', '', '', '', '', '', '', True

    if requirements['regions'] == 'off':

        dfregions = pd.DataFrame()
        dfregions_m = pd.DataFrame()
        gene_region_criteria = {}

        #1) Generate WHOLE GENE PANEL
        dfpanel, tiergenes = panelg.newpanel(dfsample.to_dict(orient='records'), drivers, dfttypedesc, gene_CDS_enst[gene_CDS_enst['Gene symbol'].isin(drivers)][['Gene symbol','CDSlen']].to_dict(orient='records'),stringent_tier1)
        dfpanel_CDS = pd.merge(gene_CDS_enst[['Gene symbol','CDSlen']].drop_duplicates(), dfpanel, on='Gene symbol',how='right')
        logger.info('>>>>>>>>>>>>>>> Panel generated (%s)' % human(start))

        #Trimmer panel according requirements of: max number of genes and max bp number of kbp
        #Start from top-gene Tier 1 and include everything until threshold (update dfpanal and tiergenes)
        dfpanel_CDS, tiergenes = requirements_funcs.trimmering_panel_length(dfpanel_CDS, tiergenes, requirements)
        logger.info('>>>>>>>>>>>>>>> Timmering panel length (%s)' % human(start))

        prescriptions_count, prescriptions_items = panel_prescriptions.take_prescription(dfpanel_CDS, prescriptions)
        logger.info('>>>>>>>>>>>>>>> Adding prescriptions to panel (%s)' % human(start))

        if len(dfpanel_CDS) == 0: #Previous trimmering left us without genes to consider for the panel -> warning
            return '', '', '', '', '', '', '', '', '', '', True

        for gene in dfpanel_CDS['Gene symbol'].tolist():
            gene_region_criteria[gene] = 'whole_gene'

        data_to_plot, data_panel, data_to_table, data_to_plot_regions, data_to_table_genes, prescriptions_items, flags = toweb.parse_data_for_web(dfpanel_CDS, tiergenes, dfgeneatt, dfttypedesc, dfregions, True, gene_region_criteria, temp_dir, gene_CDS_enst, dfsample, prescriptions_count, prescriptions_items, gene_clonality)

        logger.info('>>>>>>>>>>>>>>> Data for web parsed (%s)' % human(start))
        #Generate panel summary table
        summary = toweb.summary_table(requirements, dfpanel_CDS, dfttypedesc, dfgenedriverttype, tiergenes, genes_not_found_l, flags, trimmer_customized)


    #2) MIXED PANEL
    else :
        #Look for regions of high density of mutation in previous dataset
        dfregions = take_regions.region_identification(gene_CDS_enst, dfsample, dfttypedesc)
        logger.info('>>>>>>>>>>>>>>> Regions computed (%s)' % human(start))

        #Generate whole gene - hotspot mixed panel
        gene_region_criteria = requirements_funcs.trimmerting_panel_regions(dfregions, requirements, drivers)

        dfmixed_panel, tiermixed = mixed_panel.mixed_panel_generation(dfregions, dfsample, gene_region_criteria, dfttypedesc, gene_CDS_enst, stringent_tier1)
        logger.info('>>>>>>>>>>>>>>> Panel generated (%s)' % human(start))

        dfmixed_panel, tiermixed = requirements_funcs.trimmering_panel_length(dfmixed_panel, tiermixed, requirements)
        logger.info('>>>>>>>>>>>>>>> Timmering panel length (%s)' % human(start))

        prescriptions_count, prescriptions_items = panel_prescriptions.take_prescription(dfmixed_panel, prescriptions)
        logger.info('>>>>>>>>>>>>>>> Adding prescriptions to panel (%s)' % human(start))

        data_to_plot, data_panel, data_to_table, data_to_plot_regions, data_to_table_genes, prescriptions_items, flags = toweb.parse_data_for_web(dfmixed_panel, tiermixed, dfgeneatt, dfttypedesc, dfregions, False, gene_region_criteria, temp_dir, gene_CDS_enst, dfsample, prescriptions_count, prescriptions_items, gene_clonality)
        logger.info('>>>>>>>>>>>>>>> Data for web parsed (%s)' % human(start))

        if len(dfregions) > 0:
            dfregions = dfregions[dfregions['GENE_CLUSTER_NAME'].isin(data_panel[data_panel['Tier']!='Tier3']['Gene'].tolist())]
            dfregions_m = pd.merge(dfregions,data_panel,left_on='GENE_CLUSTER_NAME',right_on='Gene',how='left')
        else:
            dfregions_m = pd.DataFrame()

        #Generate panel summary table
        summary = toweb.summary_table(requirements, dfmixed_panel, dfttypedesc, dfgenedriverttype, tiermixed, genes_not_found_l, flags, trimmer_customized)

    logger.info('>>>>>>>>>>>>>>> Summary table (%s)' % human(start))
    #Generate BED file to download
    BEDfile_to_download = toweb.BEDfile(data_to_table, dfregions, temp_dir, requirements ,gene_exons)
    logger.info('>>>>>>>>>>>>>>> BED file (%s)' % human(start))

    Excelfile_to_download = toweb.Excelfile(data_panel, dfregions, prescriptions_items, data_to_table_genes, dfttypedesc['TTYPE'].tolist(), temp_dir, requirements)
    logger.info('>>>>>>>>>>>>>>> Excel file (%s)' % human(start))

    return data_to_table, summary, data_to_plot, dfregions_m, data_to_plot_regions, data_to_table_genes, BEDfile_to_download, gene_region_criteria, prescriptions_items, Excelfile_to_download, False

# for ttype in ['LUAD', 'LUSC', 'NSCLC', 'SCLC', 'GBM', 'LGG', 'MB', 'NB', 'PA','AML','ALL', 'CLL', 'DLBCL', 'MM',
#                                                      'BLCA', 'BRCA', 'CM', 'COREAD', 'ESCA', 'HC', 'HNSC', 'OV', 'PAAD',
#                                                      'PRAD', 'RCCC', 'STAD', 'THCA', 'UCEC']:
#     if ttype == 'GBM':
#         requirements = {'paneltype': 'drugprof','gene_filter':'off','regions':'on', 'regions_coverage':'80', 'regions_CDSprop':'60', 'whole_genes':[], 'max_g': '', 'ttype_select':[ttype], 'excluded_genes':'', 'mandatory_genes': ['BRAF','KRAS','JAK2','aabs'], 'panel_name': 'My Panel', 'max_kbp': ''}
#
# # # requirements = {'paneltype': 'basic','gene_filter':'off','regions':'on', 'regions_coverage':'80', 'regions_CDSprop':'60', 'whole_genes':[], 'max_g': '', 'ttype_select':['LUAD', 'LUSC', 'NSCLC', 'SCLC', 'GBM', 'LGG', 'MB', 'NB', 'PA','AML','ALL', 'CLL', 'DLBCL', 'MM','BLCA', 'BRCA', 'CM', 'COREAD', 'ESCA', 'HC', 'HNSC', 'OV', 'PAAD','PRAD', 'RCCC', 'STAD', 'THCA', 'UCEC'], 'excluded_genes':'', 'mandatory_genes': ['BRAF','KRAS','JAK2','aabs'], 'panel_name': 'My Panel', 'max_kbp': ''}
# requirements = {'paneltype': 'basic','gene_filter':'off','regions':'off', 'regions_coverage':'80', 'regions_CDSprop':'60', 'whole_genes':[], 'max_g': '', 'ttype_select':['COREAD','LUSC'], 'excluded_genes':'', 'mandatory_genes': '', 'panel_name': 'My Panel', 'max_kbp': ''}
# generate_panel_with_requirements(Datasets(), requirements, './temp/')