/**
 * Created by carlota on 17.03.16.
 */
$(document).ready(function() {
    $('#gene_coverage_table').dataTable( {
        "order": [[ 3, "asc" ]]
    } );

    $('#gene_attribute_table').dataTable( {
        "order": [[ 1, "asc" ]]
    } );

    $('#gene_coverage_multi_table').dataTable( {
        "order": [[ 2, "asc" ]]
    } );

    $('#regions_detail_table1').dataTable( {
        "order": [[ 5, "asc" ]]
    } );

    $('#regions_detail_table2').dataTable( {
        "order": [[ 5, "asc" ]]
    } );

    $('#gene_relmuts_table').dataTable( {
        columnDefs: [ {
            targets: [ 3, 1, 5 ],
            orderData: [ 3, 1, 5 ]
        } ],
        "order": [[ 5, "desc" ]],
        "scrollX": true,
       fnDrawCallback: function (oSettings) {
            $('[data-toggle="popover"]').popover();
        }
    } );
   $('[data-toggle="popover"]').popover();



} );