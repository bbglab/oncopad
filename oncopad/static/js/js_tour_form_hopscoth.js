/* globals hopscotch: false */

/* PANEL TOUR */
var tour = {
  id: 'touring',
  steps: [
    {
      target: 'naming_panel',
      title: 'Getting started',
      content: 'Provide a name for your panel (default is My Panel).',
      placement: 'right',
      arrowOffset: 100,
      yOffset: -100
    },
    {
      target: 'cancer_type_details',
      title: 'Select a cancer type(s)',
      content: 'Select the cancer type(s) you would like to interrogate with your panel.',
      placement: 'top',
      arrowOffset: 100,
      yOffset: -50
    },    {
      target: 'multi_individualcheckbox',
      title: '',
      content: 'Tip: you can also select one or more of the default multi cancer type groups suggested.',
      placement: 'top',
      arrowOffset: 100,
      yOffset: -50
    },
    {
      target: 'genes_input_details',
      placement: 'top',
      title: 'Select genes to include in the panel',
      content: 'Select the input list of genes for designing the panel; you can choose between pre-compiled lists of driver genes or drug response biomarkers (see About for details) or construct your own list.',
      arrowOffset: 100,
      yOffset: -40
    },
    {
      target: 'button_advanced',
      title: 'Advanced settings',
      content: 'You can also fine tune panel parameters in the Advanced Settings sections. Briefly, you can define  i) which genes from pre-compiled lists are considered as panel candidates, ii) the parameters to detect gene mutational hotspots; and iii) the panel size.',
      placement: 'top',
      arrowOffset: 100,
      yOffset: -10
    }
  ],
  showPrevButton: true,
  scrollTopMargin: 100
},

/* ========== */
/* TOUR SETUP */
/* ========== */
addClickListener = function(el, fn) {
  if (el.addEventListener) {
    el.addEventListener('click', fn, false);
  }
  else {
    el.attachEvent('onclick', fn);
  }
},

init = function() {
  var startBtnId = 'tour_button',
      calloutId = 'startTourCallout',
      mgr = hopscotch.getCalloutManager(),
      state = hopscotch.getState();

  if (state && state.indexOf('touring') === 0) {
    // Already started the tour at some point!
    hopscotch.startTour(tour);
  }


  addClickListener(document.getElementById(startBtnId), function() {
    if (!hopscotch.isActive) {
      mgr.removeAllCallouts();
      hopscotch.startTour(tour);
    }
  });
};

init();

