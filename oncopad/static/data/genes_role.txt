Gene symbol	Role
PTEN	Loss of function
ROS1	Activating
NF2	Loss of function
HRAS	Activating
CDK4	No class
CDK6	Activating
ERCC2	Activating
NRAS	Activating
NOTCH1	Loss of function
CCND2	Activating
NOTCH2	Loss of function
FGFR2	Activating
MSH2	Unknown
GNAQ	Unknown
AR	Unknown
MLL	Loss of function
ATM	Loss of function
CD79B	Unknown
CCND1	Activating
ERBB3	Activating
KRAS	Activating
MYD88	No class
AKT2	Unknown
APC	Loss of function
FGF4	Unknown
BRCA1	Loss of function
BRCA2	Loss of function
TSC1	Loss of function
ARAF1	Unknown
IDH2	Activating
JAK2	Unknown
FGF3	Unknown
PIK3CA	Activating
DDR2	Unknown
FGFR1	No class
TP53	Loss of function
AURKA	Activating
IDH1	Activating
GNA11	Activating
PML	Unknown
TSC2	Unknown
FGFR3	Activating
AKT1	Activating
CDKN2A	Loss of function
NF1	Loss of function
EZH2	Loss of function
ATR	No class
MYCN	No class
nan	Unknown
KCNJ5	Unknown
AKT3	Activating
FBXW7	Loss of function
ARID1A	Loss of function
HGF	No class
ER	Unknown
 ERBB2	Unknown
EGFR	Activating
ABL1	Activating
ALK	Activating
BRAF	Activating
ERBB2	No class
MTOR	Activating
BTK	Unknown
KIT	Activating
PDGFRA	Unknown
PDGFRB	Unknown
KDR	Activating
NCKAP1	Loss of function
AXIN2	Loss of function
CHD9	Activating
ARHGEF2	Activating
UPF3B	Loss of function
ARID4B	Loss of function
TJP1	Loss of function
TBL1XR1	Loss of function
HDAC3	Loss of function
WNK1	No class
AHNAK	Activating
AHCTF1	No class
PER1	No class
CAST	Loss of function
CNTNAP1	No class
EIF4A2	Activating
FANCI	No class
AHR	Activating
PABPC3	No class
RAD21	Loss of function
PIK3R3	Activating
MAP2K4	Loss of function
CEP290	Loss of function
PRKCZ	No class
LPHN2	Activating
ARHGEF6	Loss of function
NCOR1	Loss of function
MDM2	Activating
FAM123B	Loss of function
BRWD1	Activating
PPM1D	Loss of function
GPSM2	Loss of function
PHF6	Loss of function
PGR	Activating
ARHGAP29	No class
CCNE1	Activating
MYB	Loss of function
HLA-B	Loss of function
RUNX1	Loss of function
STK11	Loss of function
TOM1	Loss of function
MYH9	No class
RET	Activating
CHD3	Loss of function
HSPA8	Activating
CRTC3	No class
CHD8	Loss of function
MAP4K3	Loss of function
SCAI	Activating
SOS1	Activating
TJP2	Activating
MNDA	No class
EIF1AX	Loss of function
SEC31A	Loss of function
HSP90AA1	Activating
COL1A1	Activating
LIMA1	Loss of function
HSP90AB1	Loss of function
MED24	No class
PTPRF	Activating
INPP4A	Loss of function
TRERF1	Activating
PTPRU	Activating
DLG1	Activating
ARAP3	Loss of function
DEPDC1B	Loss of function
BAZ2B	Activating
IGF1R	Activating
MCM3	Activating
MAP2K1	Activating
TBX3	Loss of function
SMAD4	Loss of function
STK4	No class
SPRR3	Activating
MLH1	Loss of function
PSMD11	Loss of function
CLOCK	Loss of function
CYTH4	No class
KDM6A	Loss of function
PCSK5	No class
NUP107	Activating
AKAP9	No class
CSDA	Activating
FLT3	Activating
SYK	No class
MAT2A	No class
SMARCA1	Loss of function
CNOT4	No class
NTRK2	Activating
LAMA2	Loss of function
MGA	Loss of function
ERBB2IP	Loss of function
NCOR2	Activating
ARFGEF2	Activating
MLL3	Loss of function
RPGR	Loss of function
MDM4	Activating
ITSN1	Loss of function
TP53BP1	Loss of function
TCF12	Loss of function
FBXO11	No class
MYH14	Activating
ARFGEF1	No class
ZFHX3	Loss of function
ARID4A	No class
ASH1L	Activating
GNG2	Loss of function
SUZ12	Loss of function
CBFB	Loss of function
PCDH18	Activating
DDX5	Loss of function
TXNIP	Activating
CLSPN	Loss of function
ELF1	Loss of function
RBM5	Loss of function
G3BP1	No class
MAP4K1	No class
ZNF638	No class
ARNTL	Loss of function
CLCC1	Activating
IRF7	Activating
MED23	Loss of function
ARID1B	Loss of function
WHSC1L1	Loss of function
FOXA1	Loss of function
FUS	Loss of function
HLA-A	Loss of function
IREB2	Activating
ACTG1	Activating
ATF1	No class
PIP5K1A	No class
DDX3X	Loss of function
MYH10	Activating
CHD6	Activating
RPSAP58	Activating
MCM8	Activating
POM121	Activating
ZNF814	Activating
NDRG1	Activating
CNOT3	Activating
RBM10	Loss of function
TAOK1	No class
FXR1	Loss of function
BAP1	Loss of function
CCAR1	Loss of function
PRRX1	Activating
RAD23B	Loss of function
SHMT1	No class
PRPF8	Activating
MMP2	Activating
STAG2	Loss of function
PPP2R5A	Loss of function
CARM1	Activating
ACTG2	Activating
HLF	Activating
ADCY1	Activating
ROBO2	Activating
MYH11	Activating
EIF4G1	Activating
CAT	Activating
FMR1	Loss of function
RHOT1	No class
MAP3K1	Loss of function
PIK3R1	Loss of function
SEC24D	Loss of function
ACVR1B	Loss of function
CASP1	Loss of function
MAP3K11	No class
FKBP5	Activating
CDKN1A	Loss of function
FAT1	Loss of function
NAP1L1	Loss of function
RHOA	Activating
TCF7L2	Loss of function
CASP8	Loss of function
MAP3K4	Activating
RPL22	Loss of function
CSNK2A1	Loss of function
IRF6	No class
EPHA4	No class
RBBP7	Activating
F8	Activating
ZC3H11A	No class
CHD1L	No class
MFNG	Activating
IRS2	Loss of function
ZNF750	Loss of function
FN1	Loss of function
TAOK2	Activating
KEAP1	Loss of function
FUBP1	Loss of function
CDKN2B	Loss of function
TFDP2	Loss of function
FAM46C	No class
GNAS	Activating
PAX5	Activating
FIP1L1	Activating
APAF1	Loss of function
FAT2	Activating
POLR2B	Activating
NSD1	Loss of function
LRPPRC	Loss of function
CRNKL1	Activating
NUP98	Activating
PBRM1	Loss of function
XRN1	No class
ARFGAP1	No class
FRG1	Loss of function
PLCB1	Activating
ATP6AP2	Loss of function
NCF2	No class
ATRX	Loss of function
PIK3C2B	No class
SF3B1	Activating
TGFBR2	Loss of function
G3BP2	Activating
CAD	Activating
CREBBP	Loss of function
CYLD	Loss of function
RASA1	Loss of function
KLF4	No class
RTN4	Loss of function
CTTN	Activating
CTCF	Loss of function
COPS2	Loss of function
ACO1	Loss of function
FOXA2	Loss of function
TRIP10	Activating
ELF3	Loss of function
CSDE1	Loss of function
CUL2	Loss of function
CSNK1G3	Loss of function
SMO	No class
USP6	No class
ACAD8	Activating
SRGAP1	Activating
TRIO	Activating
ARHGAP26	Loss of function
NFATC4	Loss of function
EPHA1	Activating
NR2F2	Activating
TET2	Loss of function
LCP1	Loss of function
MECOM	Activating
VIM	No class
YBX1	Activating
FAS	Loss of function
EEF1A1	Loss of function
KLF6	Loss of function
PPP6C	Loss of function
CDH1	Loss of function
SMC1A	Activating
ITGA9	Activating
RASGRP1	No class
EP300	Loss of function
SMARCB1	Loss of function
PPP2R1A	Activating
MSR1	Loss of function
CDK12	Loss of function
CHD4	Activating
ARFGAP3	Loss of function
ARHGAP35	Loss of function
SPTAN1	Loss of function
RPL5	Loss of function
DHX15	Loss of function
WT1	Loss of function
MAGI2	No class
NUP93	Loss of function
BPTF	Activating
CEBPA	Loss of function
CNOT1	Loss of function
RGS3	No class
SYNCRIP	No class
SIN3A	Activating
SF3A3	Loss of function
SOX17	Activating
HNRPDL	Loss of function
TCF4	Activating
SETDB1	Activating
RAF1	Activating
PCSK6	Activating
ZFP36L2	Loss of function
MGMT	Activating
MED12	Activating
CDKN1B	Loss of function
ASPM	Activating
NCK1	Activating
PTPN11	Activating
MLLT4	Loss of function
PLXNB2	Activating
KALRN	Activating
MAX	Activating
THRAP3	Loss of function
MLH3	Loss of function
RASA2	Loss of function
KAT6B	Activating
ELF4	Activating
ABL2	Activating
ANK3	Activating
STARD13	Activating
SPOP	No class
EIF2C3	Activating
WHSC1	No class
ARID5B	Loss of function
GATA3	Loss of function
HNF1A	Activating
PPP2R5C	Activating
SH2B3	No class
BNC2	Loss of function
SOS2	Activating
PSIP1	Loss of function
LNPEP	No class
PABPC1	No class
CIC	Loss of function
RHEB	Activating
GNAI1	Loss of function
NTRK1	Activating
BCLAF1	Activating
SOX9	Loss of function
DHX9	Activating
EFTUD2	No class
SMARCA4	Loss of function
GOLGA5	No class
ACVR2A	Loss of function
NTRK3	Activating
MACF1	Activating
KDM5C	Loss of function
STAG1	Activating
BCOR	Loss of function
VHL	Loss of function
HCFC1	Activating
RAC1	Activating
DNMT3A	Loss of function
PIK3CB	No class
EEF1B2	Activating
IRF2	Loss of function
SRGAP3	Activating
SVEP1	No class
AQR	Activating
LRP6	Activating
PTGS1	Loss of function
B2M	Loss of function
EIF2AK3	Loss of function
ACSL3	No class
ATIC	Loss of function
NTN4	Activating
ZNF292	Loss of function
ASXL1	Loss of function
INPPL1	Loss of function
NPM1	Loss of function
U2AF1	Activating
FOXP1	Loss of function
PLCG1	Activating
ACACA	Activating
SFPQ	Loss of function
EPHA2	Loss of function
GPS2	Loss of function
WIPF1	Activating
SMAD2	Loss of function
ZFP36L1	Loss of function
MET	Activating
PRKAR1A	Loss of function
NEDD4L	Activating
PCBP1	Activating
CIITA	No class
ADAM10	Activating
PLXNA1	Activating
CDC73	No class
AXIN1	Loss of function
WNT5A	Activating
CUL3	Loss of function
TFDP1	Activating
FCRL4	No class
CLTC	Activating
WASF3	No class
ACSL6	Loss of function
HDAC9	Activating
TNPO2	No class
JMY	No class
CDC27	Loss of function
CLASP2	Loss of function
CCT5	Activating
RFC4	Activating
DICER1	Loss of function
MLL2	Loss of function
TNPO1	No class
NKX3-1	Activating
DHX35	Activating
CCND3	Activating
BCL11A	Activating
CAPN7	No class
EIF4G3	Activating
MEN1	Loss of function
PSME3	Loss of function
CHEK2	Loss of function
BLM	No class
ZMYM2	Loss of function
EPHB2	Activating
MEF2C	Activating
TAF1	Activating
AFF4	No class
MED17	Activating
FAF1	Loss of function
SETD2	Loss of function
BMPR2	Loss of function
MYC	No class
ARID2	Loss of function
DIS3	Activating
MKL1	Activating
C15orf55	No class
LDHA	Loss of function
CTNND1	Loss of function
EPC1	No class
NFE2L2	Activating
CUX1	No class
RB1	Loss of function
ING1	Loss of function
PTCH1	No class
SMURF2	No class
XPO1	No class
CTNNB1	Activating
MUC20	Loss of function
CUL1	Loss of function
ACTB	Activating
NR4A2	Loss of function
STIP1	Loss of function
PSMA6	No class
