[server]
host = string(default=localhost)
port = integer(default=8080)

public_url = string(default='http://localhost:8080/')


[apps]
name=string

    [[web]]
    mount_point = string(default=/)

[scheduler]
anonymous_jobs = integer(default=None)

[security]

    [[auth0]]
    AUTH0_CLIENT_ID=string
    AUTH0_DOMAIN=string
    AUTH0_CLIENT_SECRET=string
