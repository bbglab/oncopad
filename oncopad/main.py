import os
import shutil
from os import path

import bglogs
import cherrypy
from bgconfig import BGConfig
from jinja2 import Environment, FileSystemLoader
from bgweb.jobs import JobsManagerAllowingAnonymous
from bgweb.server import BGServer

from oncopad.panel_generation import generate_panel_with_requirements
from oncopad.webapp import WebApp


class Server(BGServer):

    def configure(self):
        cherrypy.config.update({'server.thread_pool': 30})


class JobsManager(JobsManagerAllowingAnonymous):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if path.exists(self.workspace):
            shutil.rmtree(self.workspace)

        if not path.exists(self.workspace):
            os.makedirs(self.workspace)

    def _load_jobs(self):
        pass

    def _get_output_folder(self, job_metadata):
        return self.workspace

    def _remove_parents(self, job_id):
        pass

    def create_job(self, requester):
        self.can_create_new_job(requester)
        if requester.isanonymous:
            self.anonymous_manager.add(requester.id)
        return

    def launch_job(self, data, requirements):
        return generate_panel_with_requirements(data, requirements=requirements, temp_dir=self.workspace)

    def remove(self, requester, job_id):
        raise NotImplementedError

    def change_metadata(self, requester, job_id, field, new_value, action='replace'):
        raise NotImplementedError

    def is_example(self, requester, job_id):
        raise NotImplementedError

    def is_public(self, requester, job_id):
        raise NotImplementedError

    def clean_anonymous_jobs(self):
        raise NotImplementedError

    def get_directory(self):
        return self.workspace


if __name__ == '__main__':

    project_directory = path.dirname(path.abspath(__file__))

    config_template = path.join(project_directory, 'conf/oncopad.conf.template')
    config_spec = path.join(project_directory, 'conf/oncopad.conf.template.spec')
    kw = {'config_spec': config_spec}
    config_file = os.environ.get('BGWEB_CONFIG', path.join(project_directory, 'conf/oncopad.conf'))
    if path.exists(config_file):
        kw['config_file'] = config_file

    conf = BGConfig(config_template, **kw)

    bglogs.configure('oncopad', debug=False)
    bglogs.configure('bgweb', debug=False)
    bglogs.configure('cherrypy.access', debug=False)
    bglogs.configure('cherrypy.error', debug=False)

    static_dirs = ['css', 'images', 'js']
    static_base_dir = path.abspath(path.join(project_directory, 'static'))
    static_dirs_dict = {}
    for dir in static_dirs:
        static_dirs_dict['/' + dir] = path.join(static_base_dir, dir)
    static_dirs_dict['/static'] = static_base_dir

    env = Environment(loader=FileSystemLoader(path.join(project_directory, 'templates')))

    scheduler_conf = conf['scheduler']
    scheduler_conf['scheduler'] = 'local'  # just a value that is not being used

    scheduler_conf['workspace'] = path.abspath(path.join(project_directory, 'temp'))
    static_dirs_dict['/temp'] = scheduler_conf['workspace']

    jobs_manager = JobsManager(scheduler_conf, conf['apps']['name'])

    web_app = WebApp(env, conf['apps']['web'], static_dirs_dict, jobs_manager)
    web_app.setup(conf)

    server = Server(conf['server'])

    server.add_app(web_app)

    server.run()
