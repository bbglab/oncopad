import os
import json
import urllib

import bglogs
import cherrypy

from bgweb import BGWeb
from bgweb.jobs import MaxAnonymousJobsError, AuthorizationError, MaxStoredJobsError, MaxConcurrentJobsError, \
    JobSubmissionDisabledError
from bgweb.profile import BGProfileError
from bgweb.tools.authentication import RegisterUsersToFileListener
from bgweb.tools.authentication.auth0 import Auth0
from bgweb.tools.authentication.utils import require_signin
from bgweb.users import UsersManager
from cherrypy.lib.static import serve_file
from fastcache import lru_cache

from oncopad import to_dict
from oncopad.panel_generation import *
from oncopad.requirements import *


logger = bglogs.get_logger(__name__)


class WebApp(BGWeb):

    def __init__(self, env, conf, static_dirs, jobs_manager):

        self.env = env

        self.data = Datasets()

        self.jobs_manager = jobs_manager

        super(WebApp, self).__init__(conf, static_dirs)

        env.globals['path_info'] = lambda: cherrypy.request.path_info

        self.server_conf['/'] = {
            'tools.gzip.on': True,
            'response.timeout': 6000,
            # 'tools.staticdir.root': os.path.abspath(os.getcwd()),
        }

        self.cancer_types_groups = {'Haematological malignancies': ['ALL', 'AML', 'CLL', 'DLBCL', 'MM'],
                                    'Central Nervous System malignancies': ['GBM', 'LGG', 'MB', 'NB', 'PA'],
                                    'Lung carcinomas': ['LUAD', 'LUSC', 'NSCLC', 'SCLC'],
                                    'Digestive System carcinomas': ['COREAD', 'ESCA', 'HC', 'PAAD', 'STAD'],
                                    'Genitourinary System malignancies': ['BLCA', 'UCEC', 'OV', 'PRAD', 'RCCC'],
                                    'Solid tumors': ['LUAD', 'LUSC', 'NSCLC', 'SCLC', 'GBM', 'LGG', 'MB', 'NB', 'PA',
                                                     'BLCA', 'BRCA', 'CM', 'COREAD', 'ESCA', 'HC', 'HNSC', 'OV', 'PAAD',
                                                     'PRAD', 'RCCC', 'STAD', 'THCA', 'UCEC']}

    def setup(self, global_conf):
        self.app_name = global_conf['apps']['name']
        self.env.globals['app_name'] = self.app_name

        self.url = urllib.parse.urljoin(global_conf['server']['public_url'], self.mount_point)

        # Enable the tools for the session and auth in the root
        self.server_conf['/']['tools.sessions.on'] = True
        self.server_conf['/']['tools.authentication.on'] = True

        auth0_conf = global_conf['security']['auth0']
        self.auth = Auth0(auth0_conf,
                          app_url=self.url,
                          login_path='/signin',
                          logout_path='/signout',
                          force_login_function=self.force_login,
                          logout_redirect='home?msg={}'.format(urllib.parse.quote_plus('Logged out'))
                          )
        cherrypy.tools.authentication = self.auth

        if global_conf['security'].get('registered_users', None) is not None:
            cherrypy.tools.authentication.add_listener(
                RegisterUsersToFileListener(global_conf['security']['registered_users']))

        self.users_manager = UsersManager(self.auth, self.app_name)
        self.env.globals['getUser'] = self.users_manager.get_registered_user

    def force_login(self, msg=None):
        """
        Function for the authentication tool

        Returns:
            str. The loging page code without changing the URL
        """
        return self.env.get_template('login.html').render(msg=msg)

    def get_user(self):
        return self.users_manager.get_user()

    @cherrypy.expose
    def index(self):
        raise cherrypy.HTTPRedirect("index_p")

    @cherrypy.expose(alias='home')
    def index_p(self, msg=None):
        return self.env.get_template('index.html').render(msg=msg)

    @cherrypy.expose
    def case_studies(self):
        return self.env.get_template('case_studies.html').render()

    @cherrypy.expose
    def about(self):
        return self.env.get_template('about.html').render()

    @cherrypy.expose
    def terms(self):
        return self.env.get_template('terms.html').render()

    @cherrypy.expose
    def design_panel(self):
        terms = False

        user = self.get_user()
        if user is None:
            raise cherrypy.HTTPError(500, 'Unknown user')

        # Do not show the page if the use cannot submit more jobs
        try:
            self.jobs_manager.can_create_new_job(user)
        except MaxStoredJobsError:
            pass  # We are not storing the jobs, so the limit does not apply
        except MaxConcurrentJobsError:
            raise cherrypy.HTTPError(403,
                                     'You have reached the maximum number of concurrent jobs permitted. Please, wait for the other jobs you have submitted.')
        except MaxAnonymousJobsError:
            return self.force_login(
                msg="You have reached the maximum number of jobs allowed without registration. "
                    "Please, register to keep using this site")
        except JobSubmissionDisabledError:
            raise cherrypy.HTTPError(503,
                                     'Job submission is currently not available due to maintenance reasons. Please, try again later.')

        profile = user.profile
        if profile is not None:
            terms = profile.terms_accepted(self.app_name)

        return self.env.get_template('design_panel.html').render(ttypes=self.data.tumor_types_without_pan(),
                                                                genes_to_search = self.data.allsymbols,
                                                                synonims_to_search = list(self.data.allsymbols_synonyms.keys()),
                                                                genes_synonims = self.data.allsymbols_synonyms,
                                                                cancer_types_groups=self.cancer_types_groups,
                                                                warning_tumortype='off', warning_genes='off',
                                                                warning_druggability='off', warning_user_genes='off',
                                                                 terms_accepted=terms)

    @cherrypy.expose
    def results_reports(self, **kargs):

        if 'run_example' in kargs.keys():
            return self.compute_example()

        # Check if submit has tumor type
        if 'ttype_select' not in kargs.keys() and kargs['multicancertype'] == 'off':
            return self.env.get_template('design_panel.html').render(ttypes=self.data.tumor_types_without_pan(),
                                                                genes_to_search = self.data.allsymbols,
                                                                synonims_to_search = list(self.data.allsymbols_synonyms.keys()),
                                                                genes_synonims = self.data.allsymbols_synonyms,
                                                                cancer_types_groups=self.cancer_types_groups,
                                                                warning_tumortype='on',
                                                                warning_genes='off',
                                                                warning_user_gene_list_missing='off',
                                                                warning_user_genes='off',
                                                                warning_user_cohort='off')

        elif 'ttype_select_multi' not in kargs.keys() and kargs['multicancertype'] == 'on':
            return self.env.get_template('design_panel.html').render(ttypes=self.data.tumor_types_without_pan(),
                                                                genes_to_search = self.data.allsymbols,
                                                                synonims_to_search = list(self.data.allsymbols_synonyms.keys()),
                                                                genes_synonims = self.data.allsymbols_synonyms,
                                                                     cancer_types_groups=self.cancer_types_groups,
                                                                     warning_tumortype='on',
                                                                     warning_genes='off',
                                                                     warning_user_gene_list_missing='off',
                                                                     warning_user_genes='off',
                                                                     warning_user_cohort='off')

        if kargs['multicancertype'] == 'user':
            if kargs['customized_cohort_file'].file is None or 'user_cohort_ttype' not in kargs.keys():
                return self.env.get_template('design_panel.html').render(ttypes=self.data.tumor_types_without_pan(),
                                                                    genes_to_search=self.data.allsymbols,
                                                                    synonims_to_search=list(
                                                                        self.data.allsymbols_synonyms.keys()),
                                                                    genes_synonims=self.data.allsymbols_synonyms,
                                                                    cancer_types_groups=self.cancer_types_groups,
                                                                    warning_tumortype='of',
                                                                    warning_genes='off',
                                                                    warning_user_gene_list_missing='off',
                                                                    warning_user_genes='off',
                                                                    warning_user_cohort='on')

        if kargs['paneltype'] == 'customized':

            if kargs['customized_genes_file'].file is None and len(kargs['customized_genes']) == 0:
                return self.env.get_template('design_panel.html').render(ttypes=self.data.tumor_types_without_pan(),
                                                                    genes_to_search = self.data.allsymbols,
                                                                    synonims_to_search = list(self.data.allsymbols_synonyms.keys()),
                                                                         cancer_types_groups=self.cancer_types_groups,
                                                                         warning_tumortype='off',
                                                                         warning_genes='off',
                                                                         warning_user_gene_list_missing='on',
                                                                         warning_user_genes='off',
                                                                         warning_user_cohort='off')

            if kargs['customized_genes_file'].file is None and 'validated_check' not in kargs:
                return self.env.get_template('design_panel.html').render(ttypes=self.data.tumor_types_without_pan(),
                                                                    genes_to_search = self.data.allsymbols,
                                                                    synonims_to_search = list(self.data.allsymbols_synonyms.keys()),
                                                                    genes_synonims = self.data.allsymbols_synonyms,
                                                                         cancer_types_groups=self.cancer_types_groups,
                                                                         warning_tumortype='off',
                                                                         warning_genes='off',
                                                                         warning_user_gene_list_missing='on',
                                                                         warning_user_genes='off',
                                                                         warning_user_cohort='off')

        parsed_args = parse_requirements_dict(kargs)

        if parsed_args['paneltype'] != 'customized' and parsed_args['multicancertype'] != 'user':
            return self.compute_and_cache(json.dumps(parsed_args))

        return self.compute_panel(parsed_args)

    @lru_cache(1)
    def compute_example(self):
        return self.compute_panel({
                'paneltype': 'basic', 'regions': 'on', 'regions_coverage': '80', 'regions_CDSprop': '60', 'whole_genes': [],
                'max_g': '', 'ttype_select': ['AML'], 'druggability': 'all', 'excluded_genes': '',
                'mandatory_genes': ['JAK2'], 'panel_name': 'Example panel', 'max_kbp': '20', 'gene_filter':'off'
            })

    @lru_cache(128)
    def compute_and_cache(self, kargs_json):
        return self.compute_panel(json.loads(kargs_json))

    def compute_panel(self, kargs):

        user = self.users_manager.get_user()
        try:
            self.jobs_manager.create_job(user)
        except (MaxStoredJobsError, MaxConcurrentJobsError, MaxAnonymousJobsError, JobSubmissionDisabledError):
            pass  # the example can be computed

        logger.info('REQUIREMENTS %s' % kargs)
        data_to_table, summary, data_to_plot, data_to_table_regions, data_to_plot_regions, data_to_table_genes, \
        BEDfile_to_download, gene_region_criteria, prescriptions_items, Excelfile_to_download, user_nogenes = \
            self.jobs_manager.launch_job(self.data, requirements=kargs)

        # Check if panel has genes
        if user_nogenes:
            return self.env.get_template('design_panel.html').render(ttypes=self.data.tumor_types_without_pan(),
                                                                genes_to_search = self.data.allsymbols,
                                                                    cancer_types_groups=self.cancer_types_groups,
                                                                    warning_tumortype='off',
                                                                    warning_genes='off',
                                                                    warning_druggability='off',
                                                                    warning_user_genes='on')
        else:
            return self.env.get_template('results_reports.html').render(
                    requirements=kargs, data_to_table=data_to_table, dfsummary=summary, data_to_plot=data_to_plot,
                    data_to_table_regions=to_dict(data_to_table_regions), data_to_plot_regions=data_to_plot_regions,
                    data_to_table_genes=to_dict(data_to_table_genes), BEDfile=BEDfile_to_download,
                    gene_region_criteria=gene_region_criteria, prescriptions_table=to_dict(prescriptions_items),
                    Excelfile=Excelfile_to_download)

    @cherrypy.expose
    def download_panel(self, filename, displayname):
        directory = self.jobs_manager.get_directory()

        download_file = os.path.join(directory, filename)
        ct = "text/tab-separated-values"
        disp = "attachment"
        return serve_file(download_file, content_type=ct, disposition=disp, name=displayname)

    @cherrypy.expose
    def download_panel_excel(self, filename, displayname):
        directory = self.jobs_manager.get_directory()
        download_file = os.path.join(directory, filename)
        disp = "attachment"
        return serve_file(download_file,  disposition=disp, name=displayname)

    @cherrypy.expose(alias="user.js")
    def userjs(self):
        cherrypy.response.headers['Content-Type'] = 'text/javascript'
        cherrypy.response.headers['Cache-Control'] = 'no-cache'
        return self.env.get_template('user.js').render()

    ## -- user profile
    @cherrypy.expose
    @require_signin()
    def update_profile(self, to, institution, newsletter='no', terms='no'):
        user = self.users_manager.get_user()
        profile = None if user is None else user.profile
        if profile is not None:
            try:
                if institution == '':
                    institution = None
                if institution != profile.institution:
                    profile.update_institution(institution)
                newsletter = True if newsletter == 'yes' else False
                if newsletter != profile.newsletter:
                    if newsletter:
                        profile.accept_newsletter()
                    else:
                        profile.reject_newsletter()
                terms = True if terms == 'yes' else False
                current_terms = profile.terms_accepted(self.app_name)
                if terms != current_terms:
                    if terms:
                        profile.accept_terms(self.app_name)
                    else:
                        profile.reject_terms(self.app_name)
            except BGProfileError as e:
                raise cherrypy.HTTPError(503, 'Sorry, something went wrong while updating your profile. Please, try later. If the error persist, contact us. Error: {}'.format(e.message))
        raise cherrypy.HTTPRedirect(to)

    @cherrypy.expose
    def login(self, page=None):
        if page is not None:
            cherrypy.session['auth_redirect_url'] = page
        raise cherrypy.HTTPRedirect(self.auth.get_login_url())

    @cherrypy.expose
    def logout(self, page=None):
        if page is not None:
            cherrypy.session['auth_redirect_url'] = page
        raise cherrypy.HTTPRedirect(self.auth.get_logout_url())

## -- Management methods
    @cherrypy.expose
    @require_signin()
    def submissions(self, status):
        """
        Enable/disable jobs submissions
        """
        user = self.users_manager.get_user()

        if status == 'on':
            try:
                self.jobs_manager.enable_jobs_submission(user)
            except AuthorizationError:
                raise cherrypy.HTTPError(403, 'Invalid permissions')
            return 'ENABLED'
        elif status == 'off':
            try:
                self.jobs_manager.disable_jobs_submission(user)
            except AuthorizationError:
                raise cherrypy.HTTPError(403, 'Invalid permissions')
            return 'DISABLED'
        else:
            raise cherrypy.HTTPError(501, 'Status {} not valid'.format(status))
