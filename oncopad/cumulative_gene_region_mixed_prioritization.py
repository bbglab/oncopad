
import pandas as pd
import oncopad.cumulative_gene_prioritization as panelg


def mixed_panel_generation(dfregions, dfsample, gene_mixed_panel, dfttypedesc, dfexonsCDS, stringent_tier1):
    dfsample_by_gene = {k: v.to_dict(orient='records') for k, v in dfsample[['Gene symbol', 'SAMPLE', 'TTYPE']].groupby(['Gene symbol'])}
    dftopanel = []
    driver_elements = []
    CDS_panel = []
    for gene, behaviour in gene_mixed_panel.items():
        if behaviour == 'whole_gene':
            dftopanel += dfsample_by_gene[gene]
            driver_elements.append(gene)
            if gene in dfexonsCDS['Gene symbol'].drop_duplicates().tolist():
                CDS_panel.append({'CDSlen':dfexonsCDS[dfexonsCDS['Gene symbol']==gene]['CDSlen'].tolist()[0],'Gene symbol':gene})
            else:
                CDS_panel.append({'CDSlen':0,'Gene symbol':gene})
        else:
            dfregions_gene = dfregions[dfregions['Gene symbol']==gene]
            todf = []
            for row in dfregions_gene.to_dict(orient='records'):
                region_name = row['GENE_CLUSTER_NAME']
                driver_elements.append(region_name)

                for sample, ttype in zip(row['SAMPLES'], row['TTYPES']):
                    todf.append({'Gene symbol':region_name, 'SAMPLE': sample, 'TTYPE':ttype})
                CDS_panel.append({'CDSlen':row['CLUSTER_LENGTH'],'Gene symbol':region_name})
            dftopanel += todf
    CDS_panel_df = pd.DataFrame(CDS_panel)

    dfmixed_panel, tiers_mixed = panelg.newpanel(dftopanel, driver_elements, dfttypedesc, CDS_panel_df[CDS_panel_df['Gene symbol'].isin(driver_elements)][['Gene symbol','CDSlen']].to_dict(orient='records'),stringent_tier1)
    dfmixed_panelCDS = pd.merge(CDS_panel_df, dfmixed_panel, on='Gene symbol')

    return dfmixed_panelCDS, tiers_mixed