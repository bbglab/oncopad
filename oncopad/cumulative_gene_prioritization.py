from collections import Counter, defaultdict

import numpy as np
import pandas as pd

import copy
from operator import itemgetter
def genes_in_tiers(labels, arr):
    slope, intercept = np.polyfit(range(1, len(labels) + 1), arr, 1)

    tiergenes = {}

    if len(labels) == 1:
        tiergenes['Tier1'] = [labels[0]]
        tiergenes['Tier2'] = []
        tiergenes['Tier3'] = []
    else:
        for t in ['Tier1', 'Tier2', 'Tier3']:
            tiergenes[t] = []
            #Tier genes
        cut = False
        for x in range(1, len(labels) + 1):
            ylineeq = (slope * x) + intercept
            ydistrib = arr[x - 1]
            max_cov = arr[x - 2]
            if ydistrib > ylineeq:
                cut = True
                tiergenes['Tier2'].append(labels[x - 1])
            if cut == True:
                if ylineeq > ydistrib:
                    tiergenes['Tier3'].append(labels[x - 1])
                else:

                    if arr[x-1] == max_cov:
                        tiergenes['Tier3'].append(labels[x - 1])
                    else:
                        tiergenes['Tier2'].append(labels[x - 1])
            if cut == False:
                tiergenes['Tier1'].append(labels[x - 1])

        tiergenes['Tier1'] = list(set(tiergenes['Tier1'])-set(tiergenes['Tier2'])-set(tiergenes['Tier3']))
        tiergenes['Tier2'] = list(set(tiergenes['Tier2'])-set(tiergenes['Tier3']))

    if len(tiergenes['Tier2']) > 0 and len(tiergenes['Tier1']) == 0:
        tiergenes['Tier1'] = tiergenes['Tier2']
        tiergenes['Tier2'] = []

    return tiergenes


def moreonesample_acumfreq(drivers, gene_to_samples, total_samples, df):

    cumulative_values_02 = [0.0]
    cumulative_values_03 = [0.0]

    samples = Counter(set(gene_to_samples[drivers[0]]))
    for i, gene in enumerate(drivers[1:], start=1):
        samples.update(set(gene_to_samples[drivers[i]]))

        greater_than_2 = [v for v in samples.values() if v >= 2]
        greater_than_3 = [v for v in greater_than_2 if v >= 3]
        cumulative_values_02.append(len(greater_than_2) / total_samples)
        cumulative_values_03.append(len(greater_than_3) / total_samples)

    df['Cumulative_2'] = np.array(cumulative_values_02)

    cumulative_values_03[1] = 0.0
    df['Cumulative_3'] = np.array(cumulative_values_03)

    return df


def newpanel(dfsample, drivers, dfttypedesc, drivers_length, stringent_tier1):

    # Prepare all aggregations
    gene_to_samples = defaultdict(list)
    ttype_samples_gene = defaultdict(int)

    for r in dfsample:
        gene_to_samples[r['Gene symbol']].append(r['SAMPLE'])
        ttype_samples_gene[(r['TTYPE'], r['Gene symbol'])] += 1

    # List of drivers without duplicates
    drivers = set(drivers)
    # A dictionary {"gene" -> "sample1, sample2, ..."}
    gene_to_samples_str = {k: ','.join(set(v)) for k, v in gene_to_samples.items()}


    total_samples = 0
    total_samples_per_ttype = defaultdict(int)
    for r in dfttypedesc.to_dict(orient='records'):
        total_samples += r['TOTAL']
        total_samples_per_ttype[r['TTYPE']] += r['TOTAL']

    gene_to_samples_updated = copy.deepcopy(gene_to_samples)
    mergedsamples = set()
    todf = []

    for i in range(len(gene_to_samples) + 1):

        if len(gene_to_samples_updated) == 0:
            break

        sorted_gene_samples = sorted([[k , len(v)] for k, v in gene_to_samples_updated.items()], key=itemgetter(1), reverse=True)
        sorted_gene_samples_val = [row[1] for row in sorted_gene_samples]
        if len(sorted_gene_samples_val) > 1 and sorted_gene_samples_val[0] == sorted_gene_samples_val[1]:
            genes_same_max = [g_v[0] for g_v in sorted_gene_samples if g_v[1] == sorted_gene_samples_val[0]]
            genes_same_max_lengths = {}
            for driver_length in drivers_length:
                if driver_length['Gene symbol'] in genes_same_max:
                    genes_same_max_lengths[driver_length['Gene symbol']] = int(driver_length['CDSlen'])
            if len(genes_same_max_lengths) == 0:
                maxvalgene = max(sorted(gene_to_samples_updated), key=lambda g: len(gene_to_samples_updated[g]))
            else:
                maxvalgene = sorted(genes_same_max_lengths.items(), key=itemgetter(1))[0][0]
        else:
            maxvalgene = max(sorted(gene_to_samples_updated), key=lambda g: len(gene_to_samples_updated[g]))

        mergedsamples.update(gene_to_samples_updated[maxvalgene])

        freq = len(mergedsamples) / total_samples
        drivers.remove(maxvalgene)

        item = {ttype+'_Frequency': ttype_samples_gene.get((ttype, maxvalgene), 0) / tt for ttype, tt in total_samples_per_ttype.items()}
        item['Gene symbol'] = maxvalgene
        item['Accumulative frequency PAN'] = freq
        item['Samples'] = gene_to_samples_str[maxvalgene]
        todf.append(item)

        gene_to_samples_updated = {k: [i for i in v if i not in mergedsamples] for k, v in gene_to_samples_updated.items()}
        gene_to_samples_updated = {k: v for k, v in gene_to_samples_updated.items() if len(v) > 0}

    for gene in drivers:
        item = {ttype+'_Frequency': ttype_samples_gene.get((ttype, gene), 0) / tt for ttype, tt in total_samples_per_ttype.items()}
        item['Gene symbol'] = gene
        item['Accumulative frequency PAN'] = freq
        item['Samples'] = gene_to_samples_str[gene]
        todf.append(item)

    df = pd.DataFrame(todf)
    df = df.sort_values(by='Accumulative frequency PAN')
    df = df.reset_index()

    # Consider more than one driver per sample
    if len(drivers) > 1:
        df = moreonesample_acumfreq(df['Gene symbol'].tolist(), gene_to_samples, total_samples, df)

    tiergenes = genes_in_tiers(df['Gene symbol'].tolist(), df['Accumulative frequency PAN'].tolist())
    if 'Tier1' in tiergenes:
        if len(tiergenes['Tier1']) > 5:
            if stringent_tier1:
                df_1 = df[df['Gene symbol'].isin(tiergenes['Tier1'])].sort_values(by='Accumulative frequency PAN')
                tiergenes_tier1 = genes_in_tiers(df_1['Gene symbol'].tolist(), df_1['Accumulative frequency PAN'].tolist())

                tiergenes['Tier1'] = tiergenes_tier1['Tier1'] + tiergenes_tier1['Tier2']
                tiergenes['Tier2'] += tiergenes_tier1['Tier3']
    return df, tiergenes

