import re

import pandas as pd
import numpy as np

from collections import defaultdict
from itertools import chain


def check_regions_mutinside(gene, row):

    row['Gene symbol'] = gene
    row['INSIDE'] = False
    row['MUT_COUNT'] = 0
    row['BIOMARKER'] = row['BIOMARKER'].replace('(','').replace(')','')
    if 'mutation' in  row['BIOMARKER']:
        row['BIOMARKER'] =  row['BIOMARKER'].split('\(')[0]

    if 'oncogenic' not in row['BIOMARKER'] and 'insertion' not in row['BIOMARKER'] and 'biallelic' not in row['BIOMARKER'] and 'activating' not in row['BIOMARKER'] and 'mutation' not in row['BIOMARKER']:
        if '  ' in row['BIOMARKER']:
            row['BIOMARKER'] = row['BIOMARKER'].replace('  ',' ')

        muts = row['BIOMARKER'].split(' ')[1]
        if 'Ex' in row['Gene symbol']:
            range_regions = row['Gene symbol'].split(' ')[1].split(':')[1].replace(')','').split('-')
            if len(range_regions) == 1:
                range_regions.append(range_regions[0])

            for m in muts.split(','):
                m_fields = re.search(r"([^0-9])([0-9]+)([^0-9]*)",m)
                if m_fields is not None:
                    if re.search('-',m):
                        if int(range_regions[0]) >= int(m.split('-')[0]) and int(range_regions[1]) <= int(m.split('-')[1]):
                            row['INSIDE'] = True
                    else:
                        if int(m_fields.group(2)) >= int(range_regions[0]) and int(m_fields.group(2)) <= int(range_regions[1]):
                            row['INSIDE'] = True
        else:
            row['INSIDE'] = True
    else:
        row['INSIDE'] = True

    return row


def take_prescription(dfpanel,prescriptions):
    item_presc = []
    prescriptions_by_gene = {k: v.to_dict(orient='records') for k, v in prescriptions.groupby('GENE')}

    df_presc = pd.DataFrame()
    for item, dfitem in dfpanel.groupby('Gene symbol'):
        samples_set = set(dfitem['Samples'].tolist()[0].split(','))

        if re.search('\(',item):
            gene = item.split(' ')[0]
        else:
            gene = item
        p_gene = prescriptions_by_gene.get(gene, None)
        total_muts = len(set(dfitem['Samples'].tolist()[0].split(',')))
        vals = {'Sensitivity':0,'Resistance':0,'Known oncogeneicity':0}
        if p_gene is not None:
            p_gene_inside = filter(lambda r:r['INSIDE'], map(lambda r: check_regions_mutinside(item, r), p_gene))

            for r in p_gene_inside:
                samps = set(r['SAMPLE_LIST'].split(','))
                m_samps = samps.intersection(samples_set)
                r['SAMPLE_SET'] = samps
                r['MUT_COUNT'] = len(m_samps)
                r['MUT_PROP'] = np.round(len(m_samps)/float(len(samples_set)),3)

            p_gene_inside_by_assoc = defaultdict(list)
            for r in filter(lambda r:r['MUT_COUNT']!=0, p_gene):
                p_gene_inside_by_assoc[r['GENERAL_ASSOCIATION']].append(r)

            if len(p_gene_inside_by_assoc) != 0:

                for assoc, dfassoc in p_gene_inside_by_assoc.items():
                    samps = set(chain(*[l['SAMPLE_SET'] for l in dfassoc]))
                    m_samps = samps.intersection(samples_set)

                    if len(m_samps) != 0:
                        vals[assoc] = len(m_samps)
                        df_presc = df_presc.append(pd.DataFrame(dfassoc))


        item_presc.append({'Gene symbol':item,'DSens_count':vals['Sensitivity'],'DResis_count':vals['Resistance'],'Known_count':vals['Known oncogeneicity'],'Total_muts':total_muts})

    presc = []
    df_presc = df_presc.fillna('')

    if len(df_presc) != 0:
        for feats,dffeats in df_presc.groupby(['Gene symbol','MUT_COUNT','MUT_PROP','BIOMARKER','GENERAL_ASSOCIATION','DRUG']):

            evidences_ttypes_set = set()
            for r in dffeats.to_dict(orient='records'):
                if r['EVIDENCE'] == 'Pre-clinical':
                    evi = 'Pre-clinical assays'
                else:
                    evi = r['EVIDENCE']
                evidences_ttypes_set.add(evi+' for '+r['TTYPES_NAME_BIOM'])

            evidences_ttypes_list = list(evidences_ttypes_set)
            if len(evidences_ttypes_list) != 1:
                evidences_ttypes = ', '.join(evidences_ttypes_list[:-1]) + ' and '+ evidences_ttypes_list[-1]
            else:
                evidences_ttypes = ', '.join(evidences_ttypes_list)

            presc.append({'Gene symbol':feats[0],'MUT_COUNT':feats[1],'MUT_PROP':feats[2],
                          'BIOMARKER':feats[3],'GENERAL_ASSOCIATION':feats[4],'DRUG':feats[5],
                          'EVIDENCES_TTYPE':evidences_ttypes})
    return pd.DataFrame(item_presc),pd.DataFrame(presc)