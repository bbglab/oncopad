import numpy as np
import tabix
import gzip
import csv

from collections import defaultdict
from intervaltree import IntervalTree


# Class representing a single Ensembl transcript
class Transcript(object):
    # Constructor
    def __init__(self, line):
        self.exons = []
        cols = line
        self.ENST = cols[0]
        self.geneSymbol = cols[1]
        self.geneID = cols[2]
        self.TRINFO = cols[3]
        self.chrom = cols[4]
        self.strand = int(cols[5])
        self.transcriptStart = int(cols[6])
        self.transcriptEnd = int(cols[7])
        self.codingStart = int(cols[8])
        self.codingStartGenomic = int(cols[9])
        self.codingEndGenomic = int(cols[10])
        # Initializing and adding exons
        for i in range(1, len(cols) - 11, 2):
            self.exons.append(Exon(int((i + 1) / 2), int(cols[10 + i]), int(cols[11 + i])))

    # Checking if a given position is within the UTR of the transcript
    def isInUTR(self, pos):
        if self.strand == 1:
            return (pos < self.codingStartGenomic) or (pos > self.codingEndGenomic)
        else:
            return (pos > self.codingStartGenomic) or (pos < self.codingEndGenomic)

    # Checking where a given genomic position is located in the transcript
    def whereIsThisPosition(self, pos):
        if (self.strand == 1 and pos < self.codingStartGenomic) or (
                self.strand == -1 and pos > self.codingStartGenomic): return '5UTR'
        if (self.strand == 1 and pos > self.codingEndGenomic) or (
                self.strand == -1 and pos < self.codingEndGenomic): return '3UTR'
        # Iterating through exons and introns and checking if genomic position is located within
        for exon in self.exons:
            if exon.index > 1 and ((self.strand == 1 and prevexonend < pos <= exon.start) or (
                    self.strand == -1 and exon.end < pos <= prevexonend)):
                if self.intronLength(exon.index) > 5 or self.intronLength(exon.index) == 3:
                    return 'In' + str(exon.index - 1) + '/' + str(exon.index)
                else:
                    return 'fsIn' + str(exon.index - 1) + '/' + str(exon.index)
            if exon.start < pos <= exon.end:
                return str(exon.index)
            prevexonend = exon.end if self.strand == 1 else exon.start

            # Getting the length of an intron, where idx is the index of the succeeding exon

    def intronLength(self, idx):
        for exon in self.exons:
            if exon.index == idx:
                if self.strand == 1:
                    return exon.start - prev
                else:
                    return prev - exon.end
            if self.strand == 1:
                prev = exon.end
            else:
                prev = exon.start


# Class representing a single exon
class Exon(object):
    # Constructor
    def __init__(self, index, start, end):
        self.index = index
        self.start = start
        self.end = end
        self.length = end - start


# Transforming a genomic position ot HGVS coordinate
def transformToCDSCoordinate(pos, transcript):
    prevExonEnd = 99999999

    # Checking if genomic position is within translated region
    if transcript.isInUTR(pos): return None

    sumOfExonLengths = -transcript.codingStart + 1
    # Iterating through exons
    for i in range(len(transcript.exons)):
        exon = transcript.exons[i]
        if i > 0:
            if transcript.strand == 1 and prevExonEnd < pos < exon.start + 1:
                return None
            elif transcript.strand == -1 and exon.end < pos < prevExonEnd:
                return None

        # Checking if genomic position is within exon
        if exon.start + 1 <= pos <= exon.end:
            if transcript.strand == 1:
                return sumOfExonLengths + pos - exon.start
            else:
                return sumOfExonLengths + exon.end - pos + 1
        # Calculating sum of exon lengths up to this point
        sumOfExonLengths += exon.length
        if transcript.strand == 1:
            prevExonEnd = exon.end
        else:
            prevExonEnd = exon.start + 1


##################################################################################################################################

TRANSCRIPTS = defaultdict(IntervalTree)
with gzip.open('static/data/ensembl70.gz','rt') as fd:
    for line in csv.reader(fd, delimiter='\t'):
        transcript = Transcript(line)
        TRANSCRIPTS[transcript.chrom][transcript.transcriptStart:transcript.transcriptEnd + 1] = transcript


def run(values):
    retdata = []

    for row in values:
        chrom = str(row['CHR'])
        start = int(row['START'])
        end = int(row['END'])
        gene = row['GENE']

        loc_start = None
        loc_end = None
        cds_end = None

        for transcript in TRANSCRIPTS[chrom][start]:
            if not transcript.data.geneSymbol == gene: continue
            loc_start = transcript.data.whereIsThisPosition(start)
            cds_start = transformToCDSCoordinate(start, transcript.data)

        for transcript in TRANSCRIPTS[chrom][end]:
            if not transcript.data.geneSymbol == gene: continue
            loc_end = transcript.data.whereIsThisPosition(end)
            cds_end = transformToCDSCoordinate(end, transcript.data)

        if loc_start == None: loc_start = '.'
        if loc_end == None: loc_end = '.'

        if cds_start == None:
            prot_start = '.'
        else:
            prot_start = np.floor((cds_start - 1) / 3) + 1
        if cds_end == None:
            prot_end = '.'
        else:
            prot_end = np.floor((cds_end - 1) / 3) + 1

        retdata.append(
                {'CHR': chrom, 'START': start, 'END': end, 'GENE': gene, 'LOC_START': loc_start, 'LOC_END': loc_end,
                 'PROT_START': prot_start, 'PROT_END': prot_end})

    return retdata