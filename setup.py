from os import path
from distutils.core import setup
from setuptools import find_packages

directory = path.dirname(path.abspath(__file__))
with open(path.join(directory, 'requirements.txt')) as f:
    required = f.read().splitlines()


setup(
    name='oncopad',
    version='1.3',
    packages=find_packages(),
    license='UPF Free Source Code',
    author='Carlota Rubio Pérez (Barcelona Biomedical Genomics Lab)',
    author_email='carlota.rubio@upf.edu',
    install_requires=required,
)