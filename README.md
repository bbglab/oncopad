OncoPAD
=======
Profiling somatic mutations in genes which may inform about tumor evolution, prognostic and treatment is becoming a standard tool in clinical oncology. 
Commercially available cancer gene panels rely on manually gathered cancer-related genes, in a “one-size-fits-many” solution. 
The design of new panels requires laborious search of literature and cancer genomics resources, with no possibility to estimate their performance on cohorts of patients. 

To our knowledge OncoPaD is the first tool aimed at the rational design of cancer gene panels. It estimates the cost-effectiveness of the designed panel on a cohort of tumors and provides reports on the importance of individual mutations for tumorigenesis or therapy. With a friendly interface and intuitive input, OncoPaD suggests researchers relevant sets of genes to be included in the panel, because prior knowledge or analyses indicate that their mutations either drive tumorigenesis or function as biomarkers of drug response. OncoPaD also provides reports on the importance of individual mutations for tumorigenesis or therapy that support the interpretation of the results obtained with the designed panel. We demonstrate in silico that OncoPaD designed panels are more cost-effective –i.e., detect a maximum fraction of tumors in the cohort by sequencing a minimum quantity of DNA– than available panels.


### License ###
OncoPaD is made available to the general public subject to certain conditions described in its LICENSE. For the avoidance of doubt, you may use the software and any data accessed through UPF software for academic, non-commercial and personal use only, and you may not copy, distribute, transmit, duplicate, reduce or alter in any way for commercial purposes, or for the purpose of redistribution, without a license from the Universitat Pompeu Fabra (UPF). Requests for information regarding a license for commercial use or redistribution of OncoPaD may be sent via e-mail to innovacio@upf.edu.